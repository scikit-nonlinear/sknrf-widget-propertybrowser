"""
This file contains the exact signatures for all functions in module
qtpropertybrowser, except for defaults which are replaced by "...".
"""

import PySide6.QtCore
import PySide6.QtWidgets

import os
import enum
from typing import Any, Callable, Optional, Tuple, Type, Union, Sequence, Dict, List, overload
from shiboken6 import Shiboken


class  Config(object):

    class Format(enum.Enum):
        RE: Config.Format = ...  # 0x0
        RE_IM: Config.Format = ...  # 0x1
        LIN_DEG: Config.Format = ...  # 0x2
        LOG_DEG: Config.Format = ...  # 0x3

    class Scale(enum.Enum):
        T: Config.Scale = ...  # 0x0
        G: Config.Scale = ...  # 0x1
        M: Config.Scale = ...  # 0x2
        K: Config.Scale = ...  # 0x3
        _: Config.Scale = ...  # 0x4
        m: Config.Scale = ...  # 0x5
        u: Config.Scale = ...  # 0x6
        n: Config.Scale = ...  # 0x7
        p: Config.Scale = ...  # 0x8

    class PkAvg(enum.Enum):
        PK: Config.PkAvg = ...  # 0x0
        AVG: Config.PkAvg = ...  # 0x1

    class Domain(enum.Enum):
        TF: Config.Domain = ...  # 0x0
        FF: Config.Domain = ...  # 0x1
        FT: Config.Domain = ...  # 0x2
        TT: Config.Domain = ...  # 0x3
        TH: Config.Domain = ...  # 0x4

    class BrowserCol(enum.Enum):
        NONE: Config.BrowserCol = ...  # 0x0
        MINIMUM: Config.BrowserCol = ...  # 0x1
        MAXIMUM: Config.BrowserCol = ...  # 0x2
        UNIT: Config.BrowserCol = ...  # 0x3
        PKAVG: Config.BrowserCol = ...  # 0x4
        FORMAT: Config.BrowserCol = ...  # 0x5
        CHECK: Config.BrowserCol = ...  # 0x6


class QtProperty(object):

    def __init__(self, manager: Any) -> None: ...


class QtBrowserItem(PySide6.QtCore.QObject):

    def __init__(self,
                 browser: Union[QtTreePropertyBrowser, QtGroupBoxPropertyBrowser, QtButtonPropertyBrowser],
                 property: QtProperty,
                 parent: QtBrowserItem) -> None: ...


class QtTreePropertyBrowser(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtGroupBoxPropertyBrowser(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtButtonPropertyBrowser(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtGroupPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtIntPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtBoolPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtDoublePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtComplexPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtVectorComplexPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtStringPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtFilePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtDatePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtTimePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtDateTimePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtCharPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtKeySequencePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtLocalePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtPointPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtPointFPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSizePropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSizeFPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtRectPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtRectFPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtEnumPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtFlagPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSizePolicyPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtFontPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtColorPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtCursorPropertyManager(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtGroupEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtIntEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSpinBoxFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSliderFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtScrollBarFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtCheckBoxFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtDoubleEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtDoubleSpinBoxFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtComplexEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtVectorComplexEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtLineEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtFileEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtDateEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtTimeEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtDateTimeEditFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtKeySequenceEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtCharEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtLocaleEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtPointEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtPointFEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSizeEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSizeFEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtRectEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtRectFEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtEnumEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtFlagEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtSizePolicyEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtFontEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtColorEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...


class QtCursorEditorFactory(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...
