// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#include "qteditorfactory.h"

#include <QtWidgets/QScrollBar>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QAbstractItemView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMenu>
#include <QtGui/QKeyEvent>
#include <QtGui/QRegularExpressionValidator>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QColorDialog>
#include <QtWidgets/QFontDialog>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QKeySequenceEdit>
#include <QtCore/QMap>
#include <QtCore/QRegularExpression>

#if defined(Q_CC_MSVC)
#    pragma warning(disable: 4786) /* MS VS 6: truncating debug info after 255 characters */
#endif

QT_BEGIN_NAMESPACE

// Set a hard coded left margin to account for the indentation
// of the tree view icon when switching to an editor

static inline void setupTreeViewEditorMargin(QLayout *lt)
{
    enum { DecorationMargin = 4 };
    if (QApplication::layoutDirection() == Qt::LeftToRight)
        lt->setContentsMargins(DecorationMargin, 0, 0, 0);
    else
        lt->setContentsMargins(0, 0, DecorationMargin, 0);
}

// ---------- EditorFactoryPrivate :
// Base class for editor factory private classes. Manages mapping of properties to editors and vice versa.

/*!
    \class EditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of EditorFactory.

    Base class for editor factory private classes. Manages mapping of properties to editors and vice versa.
*/
template <class Editor>
class EditorFactoryPrivate
{
public:

    typedef QList<Editor *> EditorList;
    typedef QMap<QtProperty *, EditorList> PropertyToEditorListMap;
    typedef QMap<Editor *, QtProperty *> EditorToPropertyMap;
    typedef QList<QComboBox *> UnitAttributeEditorList;
    typedef QList<QComboBox *> PkAvgAttributeEditorList;
    typedef QList<QComboBox *> FormatAttributeEditorList;
    typedef QList<QtDoubleEdit *> MinimumAttributeEditorList;
    typedef QList<QtDoubleEdit *> MaximumAttributeEditorList;
    typedef QList<QtBoolEdit *> CheckAttributeEditorList;
    typedef QMap<QtProperty *, UnitAttributeEditorList> PropertyToUnitAttributeEditorListMap;
    typedef QMap<QtProperty *, PkAvgAttributeEditorList> PropertyToPkAvgAttributeEditorListMap;
    typedef QMap<QtProperty *, FormatAttributeEditorList> PropertyToFormatAttributeEditorListMap;
    typedef QMap<QtProperty *, MinimumAttributeEditorList> PropertyToMinimumAttributeEditorListMap;
    typedef QMap<QtProperty *, MaximumAttributeEditorList> PropertyToMaximumAttributeEditorListMap;
    typedef QMap<QtProperty *, CheckAttributeEditorList> PropertyToCheckAttributeEditorListMap;
    typedef QMap<QComboBox *, QtProperty *> UnitAttributeEditorToPropertyMap;
    typedef QMap<QComboBox *, QtProperty *> PkAvgAttributeEditorToPropertyMap;
    typedef QMap<QComboBox *, QtProperty *> FormatAttributeEditorToPropertyMap;
    typedef QMap<QtDoubleEdit *, QtProperty *> MinimumAttributeEditorToPropertyMap;
    typedef QMap<QtDoubleEdit *, QtProperty *> MaximumAttributeEditorToPropertyMap;
    typedef QMap<QtBoolEdit *, QtProperty *> CheckAttributeEditorToPropertyMap;

    Editor *createEditor(QtProperty *property, QWidget *parent);
    QComboBox *createUnitAttributeEditor(QtProperty *property, QWidget *parent);
    QComboBox *createPkAvgAttributeEditor(QtProperty *property, QWidget *parent);
    QComboBox *createFormatAttributeEditor(QtProperty *property, QWidget *parent);
    QtDoubleEdit *createMinimumAttributeEditor(QtProperty *property, QWidget *parent);
    QtDoubleEdit *createMaximumAttributeEditor(QtProperty *property, QWidget *parent);
    QtBoolEdit *createCheckAttributeEditor(QtProperty *property, QWidget *parent);
    void initializeEditor(QtProperty *property, Editor *e);
    void initializeUnitAttributeEditor(QtProperty *property, QComboBox *e);
    void initializePkAvgAttributeEditor(QtProperty *property, QComboBox *e);
    void initializeFormatAttributeEditor(QtProperty *property, QComboBox *e);
    void initializeMinimumAttributeEditor(QtProperty *property, QtDoubleEdit *e);
    void initializeMaximumAttributeEditor(QtProperty *property, QtDoubleEdit *e);
    void initializeCheckAttributeEditor(QtProperty *property, QtBoolEdit *e);
    void slotEditorDestroyed(QObject *object);
    void slotUnitAttributeEditorDestroyed(QObject *object);
    void slotPkAvgAttributeEditorDestroyed(QObject *object);
    void slotFormatAttributeEditorDestroyed(QObject *object);
    void slotMinimumAttributeEditorDestroyed(QObject *object);
    void slotMaximumAttributeEditorDestroyed(QObject *object);
    void slotCheckAttributeEditorDestroyed(QObject *object);

    PropertyToEditorListMap  m_createdEditors;
    EditorToPropertyMap m_editorToProperty;
    PropertyToUnitAttributeEditorListMap m_createdUnitAttributeEditors;
    PropertyToPkAvgAttributeEditorListMap m_createdPkAvgAttributeEditors;
    PropertyToFormatAttributeEditorListMap m_createdFormatAttributeEditors;
    PropertyToMinimumAttributeEditorListMap m_createdMinimumAttributeEditors;
    PropertyToMaximumAttributeEditorListMap m_createdMaximumAttributeEditors;
    PropertyToCheckAttributeEditorListMap m_createdCheckAttributeEditors;
    UnitAttributeEditorToPropertyMap m_unitAttributeEditorToProperty;
    PkAvgAttributeEditorToPropertyMap m_pkAvgAttributeEditorToProperty;
    FormatAttributeEditorToPropertyMap m_formatAttributeEditorToProperty;
    MinimumAttributeEditorToPropertyMap m_minimumAttributeEditorToProperty;
    MaximumAttributeEditorToPropertyMap m_maximumAttributeEditorToProperty;
    CheckAttributeEditorToPropertyMap m_checkAttributeEditorToProperty;
};

template <class Editor>
Editor *EditorFactoryPrivate<Editor>::createEditor(QtProperty *property, QWidget *parent)
{
    Editor *editor = new Editor(parent);
    initializeEditor(property, editor);
    return editor;
}

template <class Editor>
QComboBox *EditorFactoryPrivate<Editor>::createUnitAttributeEditor(QtProperty *property, QWidget *parent)
{
    QComboBox *editor = new QComboBox(parent);
    initializeUnitAttributeEditor(property, editor);
    return editor;
}

template <class Editor>
QComboBox *EditorFactoryPrivate<Editor>::createPkAvgAttributeEditor(QtProperty *property, QWidget *parent)
{
    QComboBox *editor = new QComboBox(parent);
    initializePkAvgAttributeEditor(property, editor);
    return editor;
}

template <class Editor>
QComboBox *EditorFactoryPrivate<Editor>::createFormatAttributeEditor(QtProperty *property, QWidget *parent)
{
    QComboBox *editor = new QComboBox(parent);
    initializeFormatAttributeEditor(property, editor);
    return editor;
}

template <class Editor>
QtDoubleEdit *EditorFactoryPrivate<Editor>::createMinimumAttributeEditor(QtProperty *property, QWidget *parent)
{
    QtDoubleEdit *editor = new QtDoubleEdit(parent);
    initializeMinimumAttributeEditor(property, editor);
    return editor;
}

template <class Editor>
QtDoubleEdit *EditorFactoryPrivate<Editor>::createMaximumAttributeEditor(QtProperty *property, QWidget *parent)
{
    QtDoubleEdit *editor = new QtDoubleEdit(parent);
    initializeMaximumAttributeEditor(property, editor);
    return editor;
}

template <class Editor>
QtBoolEdit *EditorFactoryPrivate<Editor>::createCheckAttributeEditor(QtProperty *property, QWidget *parent)
{
    QtBoolEdit *editor = new QtBoolEdit(parent);
    initializeCheckAttributeEditor(property, editor);
    return editor;
}

template <class Editor>
void EditorFactoryPrivate<Editor>::initializeEditor(QtProperty *property, Editor *editor)
{
    typename PropertyToEditorListMap::iterator it = m_createdEditors.find(property);
    if (it == m_createdEditors.end())
        it = m_createdEditors.insert(property, EditorList());
    it.value().append(editor);
    m_editorToProperty.insert(editor, property);
}

template <class Editor>
void EditorFactoryPrivate<Editor>::initializeUnitAttributeEditor(QtProperty *property, QComboBox *editor)
{
    typename PropertyToUnitAttributeEditorListMap::iterator it = m_createdUnitAttributeEditors.find(property);
    if (it == m_createdUnitAttributeEditors.end())
        it = m_createdUnitAttributeEditors.insert(property, UnitAttributeEditorList());
    it.value().append(editor);
    m_unitAttributeEditorToProperty.insert(editor, property);

    editor->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
    editor->setMinimumContentsLength(1);
    editor->view()->setTextElideMode(Qt::ElideRight);
}

template <class Editor>
void EditorFactoryPrivate<Editor>::initializePkAvgAttributeEditor(QtProperty *property, QComboBox *editor)
{
    typename PropertyToPkAvgAttributeEditorListMap::iterator it = m_createdPkAvgAttributeEditors.find(property);
    if (it == m_createdPkAvgAttributeEditors.end())
        it = m_createdPkAvgAttributeEditors.insert(property, PkAvgAttributeEditorList());
    it.value().append(editor);
    m_pkAvgAttributeEditorToProperty.insert(editor, property);

    editor->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
    editor->setMinimumContentsLength(1);
    editor->view()->setTextElideMode(Qt::ElideRight);
}

template <class Editor>
void EditorFactoryPrivate<Editor>::initializeFormatAttributeEditor(QtProperty *property, QComboBox *editor)
{
    typename PropertyToFormatAttributeEditorListMap::iterator it = m_createdFormatAttributeEditors.find(property);
    if (it == m_createdFormatAttributeEditors.end())
        it = m_createdFormatAttributeEditors.insert(property, FormatAttributeEditorList());
    it.value().append(editor);
    m_formatAttributeEditorToProperty.insert(editor, property);

    editor->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
    editor->setMinimumContentsLength(1);
    editor->view()->setTextElideMode(Qt::ElideRight);
}

template <class Editor>
void EditorFactoryPrivate<Editor>::initializeMinimumAttributeEditor(QtProperty *property, QtDoubleEdit *editor)
{
    typename PropertyToMinimumAttributeEditorListMap::iterator it = m_createdMinimumAttributeEditors.find(property);
    if (it == m_createdMinimumAttributeEditors.end())
        it = m_createdMinimumAttributeEditors.insert(property, MinimumAttributeEditorList());
    it.value().append(editor);
    m_minimumAttributeEditorToProperty.insert(editor, property);

    editor->setBounded(false);
}

template <class Editor>
void EditorFactoryPrivate<Editor>::initializeMaximumAttributeEditor(QtProperty *property, QtDoubleEdit *editor)
{
    typename PropertyToMaximumAttributeEditorListMap::iterator it = m_createdMaximumAttributeEditors.find(property);
    if (it == m_createdMaximumAttributeEditors.end())
        it = m_createdMaximumAttributeEditors.insert(property, MaximumAttributeEditorList());
    it.value().append(editor);
    m_maximumAttributeEditorToProperty.insert(editor, property);

    editor->setBounded(false);
}

template <class Editor>
void EditorFactoryPrivate<Editor>::initializeCheckAttributeEditor(QtProperty *property, QtBoolEdit *editor)
{
    typename PropertyToCheckAttributeEditorListMap::iterator it = m_createdCheckAttributeEditors.find(property);
    if (it == m_createdCheckAttributeEditors.end())
        it = m_createdCheckAttributeEditors.insert(property, CheckAttributeEditorList());
    it.value().append(editor);
    m_checkAttributeEditorToProperty.insert(editor, property);
    editor->setTextVisible(false);
}

template <class Manager>
void updateUnit(Manager *manager, QtProperty *property, QComboBox * editor)
{
    QString prefix;
    QString unit = manager->unit(property);
    QStringList enumNames;
    manager->format(property) == Config::LOG_DEG? prefix = "dB" : "";
    QMap<Config::Scale, QString>::iterator i;
    for (i = Config::ScaleNameMap.begin(); i != Config::ScaleNameMap.end(); ++i) {
        enumNames << prefix + i.value() + unit;
    }

    editor->blockSignals(true);
    editor->clear();
    editor->addItems(enumNames);
    editor->setCurrentIndex(static_cast<int>(manager->scale(property)));
    editor->blockSignals(false);
}

template <class Manager>
void updatePkAvg(Manager *manager, QtProperty *property, QComboBox * editor)
{
    QStringList enumNames = Config::PkAvgNameMap.values();

    editor->blockSignals(true);
    editor->clear();
    editor->addItems(enumNames);
    editor->setCurrentIndex(static_cast<int>(manager->pkAvg(property)));
    editor->blockSignals(false);
}

template <class Manager>
void updateFormat(Manager *manager, QtProperty *property, QComboBox * editor)
{
    QStringList enumNames = Config::FormatNameMap.values();

    editor->blockSignals(true);
    editor->clear();
    editor->addItems(enumNames);
    editor->setCurrentIndex(static_cast<int>(manager->format(property)));
    editor->blockSignals(false);
}

template <class Manager>
void updateMinimum(Manager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setScale(manager->scale(property));
    editor->setFormat(manager->format(property));
    editor->setPrecision(manager->precision(property));
    editor->setRange(manager->minimum(property), manager->maximum(property));
    editor->setValue(manager->minimum(property));
    editor->blockSignals(false);
}

void updateMinimum(QtIntPropertyManager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setPrecision(manager->precision(property));
    editor->setRange(lowest, highest);
    editor->setValue(manager->minimum(property));
    editor->blockSignals(false);
}

void updateMinimum(QtComplexPropertyManager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setScale(manager->scale(property));
    editor->setFormat(manager->format(property));
    editor->setPrecision(manager->precision(property));
    editor->setRange(lowest, highest);
    editor->setValue(manager->minimum(property).real());
    editor->blockSignals(false);
}

void updateMinimum(QtQuaternionPropertyManager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setScale(manager->scale(property));
    editor->setFormat(manager->format(property));
    editor->setPrecision(manager->precision(property));
    editor->setRange(lowest, highest);
    editor->setValue(manager->minimum(property).length());
    editor->blockSignals(false);
}

template <class Manager>
void updateMaximum(Manager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setScale(manager->scale(property));
    editor->setFormat(manager->format(property));
    editor->setPrecision(manager->precision(property));
    editor->setRange(lowest, highest);
    editor->setValue(manager->maximum(property));
    editor->blockSignals(false);
}

void updateMaximum(QtIntPropertyManager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setPrecision(manager->precision(property));
    editor->setRange(lowest, highest);
    editor->setValue(manager->maximum(property));
    editor->blockSignals(false);
}

void updateMaximum(QtComplexPropertyManager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setScale(manager->scale(property));
    editor->setFormat(manager->format(property));
    editor->setPrecision(manager->precision(property));
    editor->setRange(lowest, highest);
    editor->setValue(manager->maximum(property).real());
    editor->blockSignals(false);
}

void updateMaximum(QtQuaternionPropertyManager *manager, QtProperty *property, QtDoubleEdit *editor)
{
    editor->blockSignals(true);
    editor->setScale(manager->scale(property));
    editor->setFormat(manager->format(property));
    editor->setPrecision(manager->precision(property));
    editor->setRange(lowest, highest);
    editor->setValue(manager->maximum(property).length());
    editor->blockSignals(false);
}

template <class Manager>
void updateCheck(Manager *manager, QtProperty *property, QtBoolEdit *editor)
{
    Q_UNUSED(manager);
    editor->blockSignals(true);
    editor->setChecked(manager->check(property));
    editor->blockSignals(false);
}

template <class Editor>
void EditorFactoryPrivate<Editor>::slotEditorDestroyed(QObject *object)
{
    const typename EditorToPropertyMap::iterator ecend = m_editorToProperty.end();
    for (typename EditorToPropertyMap::iterator itEditor = m_editorToProperty.begin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            Editor *editor = itEditor.key();
            QtProperty *property = itEditor.value();
            const typename PropertyToEditorListMap::iterator pit = m_createdEditors.find(property);
            if (pit != m_createdEditors.end()) {
                pit.value().removeAll(editor);
                if (pit.value().isEmpty())
                    m_createdEditors.erase(pit);
            }
            m_editorToProperty.erase(itEditor);
            return;
        }
    }
}

template <class Editor>
void EditorFactoryPrivate<Editor>::slotUnitAttributeEditorDestroyed(QObject *object)
{
    const typename UnitAttributeEditorToPropertyMap::iterator ecend = m_unitAttributeEditorToProperty.end();
    for (typename UnitAttributeEditorToPropertyMap::iterator itEditor = m_unitAttributeEditorToProperty.begin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            QComboBox *editor = itEditor.key();
            QtProperty *property = itEditor.value();
            const typename PropertyToUnitAttributeEditorListMap::iterator pit = m_createdUnitAttributeEditors.find(property);
            if (pit != m_createdUnitAttributeEditors.end()) {
                pit.value().removeAll(editor);
                if (pit.value().isEmpty())
                    m_createdUnitAttributeEditors.erase(pit);
            }
            m_unitAttributeEditorToProperty.erase(itEditor);
            return;
        }
    }
}

template <class Editor>
void EditorFactoryPrivate<Editor>::slotPkAvgAttributeEditorDestroyed(QObject *object)
{
    const typename PkAvgAttributeEditorToPropertyMap::iterator ecend = m_pkAvgAttributeEditorToProperty.end();
    for (typename PkAvgAttributeEditorToPropertyMap::iterator itEditor = m_pkAvgAttributeEditorToProperty.begin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            QComboBox *editor = itEditor.key();
            QtProperty *property = itEditor.value();
            const typename PropertyToPkAvgAttributeEditorListMap::iterator pit = m_createdPkAvgAttributeEditors.find(property);
            if (pit != m_createdPkAvgAttributeEditors.end()) {
                pit.value().removeAll(editor);
                if (pit.value().isEmpty())
                    m_createdPkAvgAttributeEditors.erase(pit);
            }
            m_pkAvgAttributeEditorToProperty.erase(itEditor);
            return;
        }
    }
}

template <class Editor>
void EditorFactoryPrivate<Editor>::slotFormatAttributeEditorDestroyed(QObject *object)
{
    const typename FormatAttributeEditorToPropertyMap::iterator ecend = m_formatAttributeEditorToProperty.end();
    for (typename FormatAttributeEditorToPropertyMap::iterator itEditor = m_formatAttributeEditorToProperty.begin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            QComboBox *editor = itEditor.key();
            QtProperty *property = itEditor.value();
            const typename PropertyToFormatAttributeEditorListMap::iterator pit = m_createdFormatAttributeEditors.find(property);
            if (pit != m_createdFormatAttributeEditors.end()) {
                pit.value().removeAll(editor);
                if (pit.value().isEmpty())
                    m_createdFormatAttributeEditors.erase(pit);
            }
            m_formatAttributeEditorToProperty.erase(itEditor);
            return;
        }
    }
}

template <class Editor>
void EditorFactoryPrivate<Editor>::slotMinimumAttributeEditorDestroyed(QObject *object)
{
    const typename MinimumAttributeEditorToPropertyMap::iterator ecend = m_minimumAttributeEditorToProperty.end();
    for (typename MinimumAttributeEditorToPropertyMap::iterator itEditor = m_minimumAttributeEditorToProperty.begin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            QtDoubleEdit *editor = itEditor.key();
            QtProperty *property = itEditor.value();
            const typename PropertyToMinimumAttributeEditorListMap::iterator pit = m_createdMinimumAttributeEditors.find(property);
            if (pit != m_createdMinimumAttributeEditors.end()) {
                pit.value().removeAll(editor);
                if (pit.value().isEmpty())
                    m_createdMinimumAttributeEditors.erase(pit);
            }
            m_minimumAttributeEditorToProperty.erase(itEditor);
            return;
        }
    }
}

template <class Editor>
void EditorFactoryPrivate<Editor>::slotMaximumAttributeEditorDestroyed(QObject *object)
{
    const typename MaximumAttributeEditorToPropertyMap::iterator ecend = m_maximumAttributeEditorToProperty.end();
    for (typename MaximumAttributeEditorToPropertyMap::iterator itEditor = m_maximumAttributeEditorToProperty.begin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            QtDoubleEdit *editor = itEditor.key();
            QtProperty *property = itEditor.value();
            const typename PropertyToMaximumAttributeEditorListMap::iterator pit = m_createdMaximumAttributeEditors.find(property);
            if (pit != m_createdMaximumAttributeEditors.end()) {
                pit.value().removeAll(editor);
                if (pit.value().isEmpty())
                    m_createdMaximumAttributeEditors.erase(pit);
            }
            m_maximumAttributeEditorToProperty.erase(itEditor);
            return;
        }
    }
}

template <class Editor>
void EditorFactoryPrivate<Editor>::slotCheckAttributeEditorDestroyed(QObject *object)
{
    const typename CheckAttributeEditorToPropertyMap::iterator ecend = m_checkAttributeEditorToProperty.end();
    for (typename CheckAttributeEditorToPropertyMap::iterator itEditor = m_checkAttributeEditorToProperty.begin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            QtBoolEdit *editor = itEditor.key();
            QtProperty *property = itEditor.value();
            const typename PropertyToCheckAttributeEditorListMap::iterator pit = m_createdCheckAttributeEditors.find(property);
            if (pit != m_createdCheckAttributeEditors.end()) {
                pit.value().removeAll(editor);
                if (pit.value().isEmpty())
                    m_createdCheckAttributeEditors.erase(pit);
            }
            m_checkAttributeEditorToProperty.erase(itEditor);
            return;
        }
    }
}

// QtGroupEditorFactory

/*!
    \class QtGroupEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtGroupEditorFactory.

    \sa QtGroupEditorFactory
*/
class QtGroupEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtGroupEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtGroupEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtGroupEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtGroupPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtGroupEditorFactory

 \brief The QtGroupEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtGroupEditorFactory::QtGroupEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtGroupPropertyManager>(parent), d_ptr(new QtGroupEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtGroupEditorFactory::~QtGroupEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtGroupEditorFactory::connectPropertyManager(QtGroupPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtGroupEditorFactory::createEditor(QtGroupPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtGroupEditorFactory::createAttributeEditor(QtGroupPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtGroupEditorFactory::disconnectPropertyManager(QtGroupPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// ------------ QtSpinBoxFactory

/*!
    \class QtSpinBoxFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtSpinBoxFactory.

    \sa QtSpinBoxFactory
*/
class QtSpinBoxFactoryPrivate : public EditorFactoryPrivate<QtSpinBox>
{
    QtSpinBoxFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtSpinBoxFactory)
public:

    void slotPropertyChanged(QtProperty *property, int value);
    void slotRangeChanged(QtProperty *property, int min, int max);
    void slotSingleStepChanged(QtProperty *property, int step);
    void slotReadOnlyChanged(QtProperty *property, bool readOnly);
    void slotSetValue(int value);
    void slotSetMinimum(int minVal);
    void slotSetMaximum(int maxVal);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtSpinBoxFactoryPrivate::slotPropertyChanged(QtProperty *property, int value)
{
    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtSpinBox *editor : it.value()) {
        if (editor->value() != value) {
            editor->blockSignals(true);
            editor->setValue(value);
            editor->blockSignals(false);
        }
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtSpinBoxFactoryPrivate::slotRangeChanged(QtProperty *property, int min, int max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtSpinBox *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min, max);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtSpinBoxFactoryPrivate::slotSingleStepChanged(QtProperty *property, int step)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtSpinBox *editor : it.value()) {
        editor->blockSignals(true);
//        editor->setSingleStep(step);
        editor->blockSignals(false);
    }
}

void QtSpinBoxFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QtSpinBox *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtSpinBoxFactoryPrivate::slotSetValue(int value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtSpinBox *, QtProperty *>::ConstIterator  ecend = m_editorToProperty.constEnd();
    for (QMap<QtSpinBox *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor !=  ecend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
    }
}

void QtSpinBoxFactoryPrivate::slotSetMinimum(double minVal)
{
    slotSetMinimum(int(minVal));
}

void QtSpinBoxFactoryPrivate::slotSetMinimum(int minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, minVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtSpinBoxFactoryPrivate::slotSetMaximum(double maxVal)
{
    slotSetMaximum(int(maxVal));
}

void QtSpinBoxFactoryPrivate::slotSetMaximum(int maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, maxVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtSpinBoxFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtSpinBoxFactory

    \brief The QtSpinBoxFactory class provides QtSpinBox widgets for
    properties created by QtIntPropertyManager objects.

    \sa QtAbstractEditorFactory, QtIntPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtSpinBoxFactory::QtSpinBoxFactory(QObject *parent)
    : QtAbstractEditorFactory<QtIntPropertyManager>(parent), d_ptr(new QtSpinBoxFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtSpinBoxFactory::~QtSpinBoxFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtSpinBoxFactory::connectPropertyManager(QtIntPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*,int,int)),
                this, SLOT(slotRangeChanged(QtProperty*,int,int)));
    connect(manager, SIGNAL(singleStepChanged(QtProperty*,int)),
                this, SLOT(slotSingleStepChanged(QtProperty*,int)));
    connect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
                this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtSpinBoxFactory::createEditor(QtIntPropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QtSpinBox *editor = d_ptr->createEditor(property, parent);
//    editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property), manager->maximum(property));
    editor->setValue(manager->value(property));
//    editor->setKeyboardTracking(false);
    editor->setReadOnly(manager->isReadOnly(property));

    connect(editor, SIGNAL(valueChanged(int)), this, SLOT(slotSetValue(int)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSpinBoxFactory::createAttributeEditor(QtIntPropertyManager *manager, QtProperty *property,
                                                 QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}
/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtSpinBoxFactory::disconnectPropertyManager(QtIntPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*,int,int)),
                this, SLOT(slotRangeChanged(QtProperty*,int,int)));
    disconnect(manager, SIGNAL(singleStepChanged(QtProperty*,int)),
                this, SLOT(slotSingleStepChanged(QtProperty*,int)));
    disconnect(manager, SIGNAL(readOnlyChanged(QtProperty *, bool)),
                this, SLOT(slotReadOnlyChanged(QtProperty *, bool)));
}


// QtIntEditFactory

/*!
    \class QtIntEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtIntEditFactory.

    \sa QtIntEditFactory
*/
class QtIntEditFactoryPrivate : public EditorFactoryPrivate<QtIntEdit>{
    QtIntEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtIntEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, int value);
    void slotRangeChanged(QtProperty *property, int min, int max);
    void slotRangeChanged(QtProperty *property, double min, double max);
    void slotReadOnlyChanged(QtProperty *property, bool readOnly);
    void slotSetValue(int value);
    void slotSetMinimum(int minVal);
    void slotSetMaximum(int maxVal);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtIntEditFactoryPrivate::slotPropertyChanged(QtProperty *property, int value)
{
    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtIntEdit *editor : it.value()) {
        if (editor->value() != value) {
            editor->blockSignals(true);
            editor->setValue(value);
            editor->blockSignals(false);
        }
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtIntEditFactoryPrivate::slotRangeChanged(QtProperty *property, double min, double max)
{
    slotRangeChanged(property, int(min), int(max));
}

void QtIntEditFactoryPrivate::slotRangeChanged(QtProperty *property, int min, int max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtIntEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min, max);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtIntEditFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QtIntEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtIntEditFactoryPrivate::slotSetValue(int value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtIntEdit *, QtProperty *>::ConstIterator itcend = m_editorToProperty.constEnd();
    for (QMap<QtIntEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
    }
}

void QtIntEditFactoryPrivate::slotSetMinimum(double minVal)
{
    slotSetMinimum(int(minVal));
}

void QtIntEditFactoryPrivate::slotSetMinimum(int minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, minVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtIntEditFactoryPrivate::slotSetMaximum(double maxVal)
{
    slotSetMaximum(int(maxVal));
}

void QtIntEditFactoryPrivate::slotSetMaximum(int maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, maxVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtIntEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}
/*! \class QtIntEditFactory

 \brief The QtIntEditFactory class provides QtComplexEdit
 widgets for properties created by QtDoublePropertyManager objects.

 \sa QtAbstractEditorFactory, QtDoublePropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtIntEditFactory::QtIntEditFactory(QObject *parent)
: QtAbstractEditorFactory<QtIntPropertyManager>(parent), d_ptr(new QtIntEditFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtIntEditFactory::~QtIntEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtIntEditFactory::connectPropertyManager(QtIntPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*, int)),
            this, SLOT(slotPropertyChanged(QtProperty*, int)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*, int, int)),
            this, SLOT(slotRangeChanged(QtProperty*, int, int)));
    connect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
            this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtIntEditFactory::createEditor(QtIntPropertyManager *manager, QtProperty *property, QWidget *parent)
{
    QtIntEdit *editor = d_ptr->createEditor(property, parent);
    //editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property), manager->maximum(property));
    editor->setValue(manager->value(property));
    //editor->setKeyboardTracking(false);
    editor->setReadOnly(manager->isReadOnly(property));

    connect(editor, SIGNAL(valueChanged(int)), this, SLOT(slotSetValue(int)));
    connect(editor, SIGNAL(destroyed(QObject*)),
            this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtIntEditFactory::createAttributeEditor(QtIntPropertyManager *manager, QtProperty *property,
                                                 QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtIntEditFactory::disconnectPropertyManager(QtIntPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*, int)),
               this, SLOT(slotPropertyChanged(QtProperty*, int)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*, int, int)),
               this, SLOT(slotRangeChanged(QtProperty*, int, int)));
    disconnect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
               this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

// QtSliderFactory

/*!
    \class QtSliderFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtSliderFactory.

    \sa QtSliderFactory
*/
class QtSliderFactoryPrivate : public EditorFactoryPrivate<QtSlider>
{
    QtSliderFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtSliderFactory)
public:
    void slotPropertyChanged(QtProperty *property, int value);
    void slotRangeChanged(QtProperty *property, int min, int max);
    void slotSingleStepChanged(QtProperty *property, int step);
    void slotSetValue(int value);
    void slotSetMinimum(int minVal);
    void slotSetMaximum(int maxVal);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtSliderFactoryPrivate::slotPropertyChanged(QtProperty *property, int value)
{
    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtSlider *editor : it.value()) {
        editor->blockSignals(true);
        editor->setValue(value);
        editor->blockSignals(false);
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtSliderFactoryPrivate::slotRangeChanged(QtProperty *property, int min, int max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtSlider *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min, max);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtSliderFactoryPrivate::slotSingleStepChanged(QtProperty *property, int step)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtSlider *editor : it.value()) {
        editor->blockSignals(true);
//        editor->setSingleStep(step);
        editor->blockSignals(false);
    }
}

void QtSliderFactoryPrivate::slotSetValue(int value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtSlider *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QtSlider *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor ) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
    }
}

void QtSliderFactoryPrivate::slotSetMinimum(double minVal)
{
    slotSetMinimum(int(minVal));
}

void QtSliderFactoryPrivate::slotSetMinimum(int minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, minVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtSliderFactoryPrivate::slotSetMaximum(double maxVal)
{
    slotSetMaximum(int(maxVal));
}

void QtSliderFactoryPrivate::slotSetMaximum(int maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, maxVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtSliderFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtSliderFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtSliderFactory class provides QtSlider widgets for
    properties created by QtIntPropertyManager objects.

    \sa QtAbstractEditorFactory, QtIntPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtSliderFactory::QtSliderFactory(QObject *parent)
    : QtAbstractEditorFactory<QtIntPropertyManager>(parent), d_ptr(new QtSliderFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtSliderFactory::~QtSliderFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtSliderFactory::connectPropertyManager(QtIntPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*,int,int)),
                this, SLOT(slotRangeChanged(QtProperty*,int,int)));
    connect(manager, SIGNAL(singleStepChanged(QtProperty*,int)),
                this, SLOT(slotSingleStepChanged(QtProperty*,int)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtSliderFactory::createEditor(QtIntPropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QtSlider *editor = new QtSlider(Qt::Horizontal, parent);
    d_ptr->initializeEditor(property, editor);
//    editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property), manager->maximum(property));
    editor->setValue(manager->value(property));

    connect(editor, SIGNAL(valueChanged(int)), this, SLOT(slotSetValue(int)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSliderFactory::createAttributeEditor(QtIntPropertyManager *manager, QtProperty *property,
                                                QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtSliderFactory::disconnectPropertyManager(QtIntPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*,int,int)),
                this, SLOT(slotRangeChanged(QtProperty*,int,int)));
    disconnect(manager, SIGNAL(singleStepChanged(QtProperty*,int)),
                this, SLOT(slotSingleStepChanged(QtProperty*,int)));
}

// QtSliderFactory

/*!
    \class QtScrollBarFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtScrollBarFactory.

    \sa QtScrollBarFactory
*/
class QtScrollBarFactoryPrivate : public  EditorFactoryPrivate<QScrollBar>
{
    QtScrollBarFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtScrollBarFactory)
public:
    void slotPropertyChanged(QtProperty *property, int value);
    void slotRangeChanged(QtProperty *property, int min, int max);
    void slotSingleStepChanged(QtProperty *property, int step);
    void slotSetValue(int value);
    void slotSetMinimum(int minVal);
    void slotSetMaximum(int maxVal);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtScrollBarFactoryPrivate::slotPropertyChanged(QtProperty *property, int value)
{
    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    for (QScrollBar *editor : it.value()) {
        editor->blockSignals(true);
        editor->setValue(value);
        editor->blockSignals(false);
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtScrollBarFactoryPrivate::slotRangeChanged(QtProperty *property, int min, int max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtIntPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QScrollBar *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min, max);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtScrollBarFactoryPrivate::slotSingleStepChanged(QtProperty *property, int step)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QScrollBar *editor : it.value()) {
        editor->blockSignals(true);
        editor->setSingleStep(step);
        editor->blockSignals(false);
    }
}

void QtScrollBarFactoryPrivate::slotSetValue(int value)
{
    QObject *object = q_ptr->sender();
    const QMap<QScrollBar *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QScrollBar *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtScrollBarFactoryPrivate::slotSetMinimum(double minVal)
{
    slotSetMinimum(int(minVal));
}

void QtScrollBarFactoryPrivate::slotSetMinimum(int minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, minVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtScrollBarFactoryPrivate::slotSetMaximum(double maxVal)
{
    slotSetMaximum(int(maxVal));
}

void QtScrollBarFactoryPrivate::slotSetMaximum(int maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, maxVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtScrollBarFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtIntPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtScrollBarFactory

    \brief The QtScrollBarFactory class provides QScrollBar widgets for
    properties created by QtIntPropertyManager objects.

    \sa QtAbstractEditorFactory, QtIntPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtScrollBarFactory::QtScrollBarFactory(QObject *parent)
    : QtAbstractEditorFactory<QtIntPropertyManager>(parent), d_ptr(new QtScrollBarFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtScrollBarFactory::~QtScrollBarFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtScrollBarFactory::connectPropertyManager(QtIntPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*,int,int)),
                this, SLOT(slotRangeChanged(QtProperty*,int,int)));
    connect(manager, SIGNAL(singleStepChanged(QtProperty*,int)),
                this, SLOT(slotSingleStepChanged(QtProperty*,int)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtScrollBarFactory::createEditor(QtIntPropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QScrollBar *editor = new QScrollBar(Qt::Horizontal, parent);
    d_ptr->initializeEditor(property, editor);
    editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property), manager->maximum(property));
    editor->setValue(manager->value(property));
    connect(editor, SIGNAL(valueChanged(int)), this, SLOT(slotSetValue(int)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtScrollBarFactory::createAttributeEditor(QtIntPropertyManager *manager, QtProperty *property,
                                                   QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtScrollBarFactory::disconnectPropertyManager(QtIntPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*,int,int)),
                this, SLOT(slotRangeChanged(QtProperty*,int,int)));
    disconnect(manager, SIGNAL(singleStepChanged(QtProperty*,int)),
                this, SLOT(slotSingleStepChanged(QtProperty*,int)));
}

// QtCheckBoxFactory

/*!
    \class QtCheckBoxFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtCheckBoxFactory.

    \sa QtCheckBoxFactory
*/
class QtCheckBoxFactoryPrivate : public EditorFactoryPrivate<QtBoolEdit>
{
    QtCheckBoxFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtCheckBoxFactory)
public:
    void slotPropertyChanged(QtProperty *property, bool value);
    void slotSetValue(bool value);
    void slotSetCheck(bool check);
};

void QtCheckBoxFactoryPrivate::slotPropertyChanged(QtProperty *property, bool value)
{
    QtBoolPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    for (QtBoolEdit *editor : it.value()) {
        editor->blockCheckBoxSignals(true);
        editor->setChecked(value);
        editor->blockCheckBoxSignals(false);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtCheckBoxFactoryPrivate::slotSetValue(bool value)
{
    QObject *object = q_ptr->sender();

    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend;  ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtBoolPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtCheckBoxFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtBoolPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtCheckBoxFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtCheckBoxFactory class provides QCheckBox widgets for
    properties created by QtBoolPropertyManager objects.

    \sa QtAbstractEditorFactory, QtBoolPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtCheckBoxFactory::QtCheckBoxFactory(QObject *parent)
    : QtAbstractEditorFactory<QtBoolPropertyManager>(parent), d_ptr(new QtCheckBoxFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtCheckBoxFactory::~QtCheckBoxFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtCheckBoxFactory::connectPropertyManager(QtBoolPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,bool)),
                this, SLOT(slotPropertyChanged(QtProperty*,bool)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtCheckBoxFactory::createEditor(QtBoolPropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QtBoolEdit *editor = d_ptr->createEditor(property, parent);
    editor->setChecked(manager->value(property));

    connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetValue(bool)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtCheckBoxFactory::createAttributeEditor(QtBoolPropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)),
                this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtCheckBoxFactory::disconnectPropertyManager(QtBoolPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,bool)),
                this, SLOT(slotPropertyChanged(QtProperty*,bool)));
}

// QtDoubleSpinBoxFactory

/*!
    \class QtDoubleSpinBoxFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtDoubleSpinBoxFactory.

    \sa QtDoubleSpinBoxFactory
*/
class QtDoubleSpinBoxFactoryPrivate : public EditorFactoryPrivate<QtDoubleSpinBox>
{
    QtDoubleSpinBoxFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtDoubleSpinBoxFactory)
public:

    void slotPropertyChanged(QtProperty *property, double value);
    void slotRangeChanged(QtProperty *property, double min, double max);
    void slotSingleStepChanged(QtProperty *property, double step);
    void slotPrecisionChanged(QtProperty *property, int prec);
    void slotReadOnlyChanged(QtProperty *property, bool readOnly);
    void slotSetValue(double value);
    void slotSetScale(int scaleSelection);
    void slotSetFormat(int formatSelection);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtDoubleSpinBoxFactoryPrivate::slotPropertyChanged(QtProperty *property, double value)
{
    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtDoubleSpinBox *editor : it.value()) {
        editor->setFormat(manager->format(property));
        editor->setScale(manager->scale(property));
        if (!isclose(value, editor->value(), std::numeric_limits<double>::epsilon(), std::numeric_limits<double>::min())){
            editor->blockSignals(true);
            editor->setValue(value);
            editor->blockSignals(false);
        }
    }
    QList<QComboBox *> unitEditors = m_createdUnitAttributeEditors[property];
    for (unsigned short index = 0; index < unitEditors.size(); ++index ){
        updateUnit(manager, property, unitEditors[index]);
    }
    QList<QComboBox *> formatEditors = m_createdFormatAttributeEditors[property];
    for (unsigned short index = 0; index < formatEditors.size(); ++index ){
        updateFormat(manager, property, formatEditors[index]);
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotRangeChanged(QtProperty *property, double min, double max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtDoubleSpinBox *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min, max);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotSingleStepChanged(QtProperty *property, double step)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtDoubleSpinBox *editor : it.value()) {
        editor->blockSignals(true);
//        editor->setSingleStep(step);
        editor->blockSignals(false);
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QtDoubleSpinBox *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotPrecisionChanged(QtProperty *property, int prec)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtDoubleSpinBox *editor : it.value()) {
        editor->blockSignals(true);
        editor->setPrecision(prec);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotSetValue(double value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleSpinBox *, QtProperty *>::ConstIterator itcend = m_editorToProperty.constEnd();
    for (QMap<QtDoubleSpinBox *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotSetScale(int scaleSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_unitAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_unitAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setScale(property, Config::Scale(scaleSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotSetFormat(int formatSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_formatAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_formatAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setFormat(property, Config::Format(formatSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotSetMinimum(double minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, minVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotSetMaximum(double maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, maxVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleSpinBoxFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*! \class QtDoubleSpinBoxFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

 \brief The QtDoubleSpinBoxFactory class provides QtDoubleSpinBox
 widgets for properties created by QtDoublePropertyManager objects.
\sa QtAbstractEditorFactory, QtDoublePropertyManager
*/

/*!
 Creates a factory with the given \a parent.
*/
QtDoubleSpinBoxFactory::QtDoubleSpinBoxFactory(QObject *parent)
    : QtAbstractEditorFactory<QtDoublePropertyManager>(parent), d_ptr(new QtDoubleSpinBoxFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
 Destroys this factory, and all the widgets it has created.
*/
QtDoubleSpinBoxFactory::~QtDoubleSpinBoxFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
*/
void QtDoubleSpinBoxFactory::connectPropertyManager(QtDoublePropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,double)),
                this, SLOT(slotPropertyChanged(QtProperty*,double)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*,double,double)),
                this, SLOT(slotRangeChanged(QtProperty*,double,double)));
    connect(manager, SIGNAL(singleStepChanged(QtProperty*,double)),
                this, SLOT(slotSingleStepChanged(QtProperty*,double)));
    connect(manager, SIGNAL(precisionChanged(QtProperty*,int)),
                this, SLOT(slotPrecisionChanged(QtProperty*,int)));
    connect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
                this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtDoubleSpinBoxFactory::createEditor(QtDoublePropertyManager *manager,
        QtProperty *property, QWidget *parent)
{
    QtDoubleSpinBox *editor = d_ptr->createEditor(property, parent);
    //editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property), manager->maximum(property));
    editor->setPrecision(manager->precision(property));
    editor->setValue(manager->value(property));
    editor->setFormat(manager->format(property));
    editor->setScale(manager->scale(property));
    //editor->setKeyboardTracking(false);
    editor->setReadOnly(manager->isReadOnly(property));

    connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetValue(double)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtDoubleSpinBoxFactory::createAttributeEditor(QtDoublePropertyManager *manager, QtProperty *property,
                                                       QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::UNIT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::UNIT))
            return nullptr;
        QComboBox *editor = d_ptr->createUnitAttributeEditor(property, parent);
        updateUnit(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetScale(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotUnitAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::FORMAT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::FORMAT))
            return nullptr;
        QComboBox *editor = d_ptr->createFormatAttributeEditor(property, parent);
        updateFormat(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetFormat(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotFormatAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
*/
void QtDoubleSpinBoxFactory::disconnectPropertyManager(QtDoublePropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,double)),
               this, SLOT(slotPropertyChanged(QtProperty*,double)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*,double,double)),
               this, SLOT(slotRangeChanged(QtProperty*,double,double)));
    disconnect(manager, SIGNAL(singleStepChanged(QtProperty*,double)),
               this, SLOT(slotSingleStepChanged(QtProperty*,double)));
    disconnect(manager, SIGNAL(precisionChanged(QtProperty*,int)),
               this, SLOT(slotPrecisionChanged(QtProperty*,int)));
    disconnect(manager, SIGNAL(readOnlyChanged(QtProperty*,bool)),
               this, SLOT(slotReadOnlyChanged(QtProperty*,bool)));
}

// QtDoubleEditFactory

/*!
    \class QtDoubleSpinBoxFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtDoubleSpinBoxFactory.

    \sa QtDoubleSpinBoxFactory
*/
class QtDoubleEditFactoryPrivate : public EditorFactoryPrivate<QtDoubleEdit>
{
    QtDoubleEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtDoubleEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, double value);
    void slotRangeChanged(QtProperty *property, double min, double max);
    void slotPrecisionChanged(QtProperty *property, int prec);
    void slotReadOnlyChanged(QtProperty *property, bool readOnly);
    void slotSetValue(double value);
    void slotSetScale(int scaleSelection);
    void slotSetFormat(int formatSelection);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtDoubleEditFactoryPrivate::slotPropertyChanged(QtProperty *property, double value)
{
    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtDoubleEdit *editor : it.value()) {
        editor->setFormat(manager->format(property));
        editor->setScale(manager->scale(property));
        if (!isclose(value, editor->value(), std::numeric_limits<double>::epsilon(), std::numeric_limits<double>::min())){
            editor->blockSignals(true);
            editor->setValue(value);
            editor->blockSignals(false);
        }
    }
    QList<QComboBox *> unitEditors = m_createdUnitAttributeEditors[property];
    for (unsigned short index = 0; index < unitEditors.size(); ++index ){
        updateUnit(manager, property, unitEditors[index]);
    }
    QList<QComboBox *> formatEditors = m_createdFormatAttributeEditors[property];
    for (unsigned short index = 0; index < formatEditors.size(); ++index ){
        updateFormat(manager, property, formatEditors[index]);
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtDoubleEditFactoryPrivate::slotRangeChanged(QtProperty *property, double min, double max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtDoubleEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min, max);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtDoubleEditFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QtDoubleEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtDoubleEditFactoryPrivate::slotPrecisionChanged(QtProperty *property, int prec)
{
    if (!m_createdEditors.contains(property))
    return;

    QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
    return;

    QList<QtDoubleEdit *> editors = m_createdEditors[property];
    QListIterator<QtDoubleEdit *> itEditor(editors);
    while (itEditor.hasNext()) {
        QtDoubleEdit *editor = itEditor.next();
        editor->blockSignals(true);
        editor->setPrecision(prec);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtDoubleEditFactoryPrivate::slotSetValue(double value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_editorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
    }
}

void QtDoubleEditFactoryPrivate::slotSetScale(int scaleSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_unitAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_unitAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setScale(property, Config::Scale(scaleSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleEditFactoryPrivate::slotSetFormat(int formatSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_formatAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_formatAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setFormat(property, Config::Format(formatSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleEditFactoryPrivate::slotSetMinimum(double minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, minVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleEditFactoryPrivate::slotSetMaximum(double maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, maxVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtDoubleEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDoublePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}
/*! \class QtDoubleEditFactory

 \brief The QtDoubleEditFactory class provides QtComplexEdit
 widgets for properties created by QtDoublePropertyManager objects.

 \sa QtAbstractEditorFactory, QtDoublePropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtDoubleEditFactory::QtDoubleEditFactory(QObject *parent)
: QtAbstractEditorFactory<QtDoublePropertyManager>(parent), d_ptr(new QtDoubleEditFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtDoubleEditFactory::~QtDoubleEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtDoubleEditFactory::connectPropertyManager(QtDoublePropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*, double)),
            this, SLOT(slotPropertyChanged(QtProperty*, double)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*, double, double)),
            this, SLOT(slotRangeChanged(QtProperty*, double, double)));
    connect(manager, SIGNAL(precisionChanged(QtProperty*, int)),
            this, SLOT(slotPrecisionChanged(QtProperty*, int)));
    connect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
            this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtDoubleEditFactory::createEditor(QtDoublePropertyManager *manager,
                                        QtProperty *property, QWidget *parent)
{
    QtDoubleEdit *editor = d_ptr->createEditor(property, parent);
    //editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property), manager->maximum(property));
    editor->setPrecision(manager->precision(property));
    editor->setValue(manager->value(property));
    editor->setFormat(manager->format(property));
    editor->setScale(manager->scale(property));
    //editor->setKeyboardTracking(false);
    editor->setReadOnly(manager->isReadOnly(property));

    connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetValue(double)));
    connect(editor, SIGNAL(destroyed(QObject *)),
            this, SLOT(slotEditorDestroyed(QObject *)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtDoubleEditFactory::createAttributeEditor(QtDoublePropertyManager *manager, QtProperty *property,
                                                    QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::UNIT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::UNIT))
            return nullptr;
        QComboBox *editor = d_ptr->createUnitAttributeEditor(property, parent);
        updateUnit(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetScale(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotUnitAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::FORMAT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::FORMAT))
            return nullptr;
        QComboBox *editor = d_ptr->createFormatAttributeEditor(property, parent);
        updateFormat(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetFormat(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotFormatAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtDoubleEditFactory::disconnectPropertyManager(QtDoublePropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*, double)),
               this, SLOT(slotPropertyChanged(QtProperty*, double)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*, double, double)),
               this, SLOT(slotRangeChanged(QtProperty*, double, double)));
    disconnect(manager, SIGNAL(precisionChanged(QtProperty*, int)),
               this, SLOT(slotPrecisionChanged(QtProperty*, int)));
    disconnect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
               this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

// QtComplexEditFactory

/*!
    \class QtComplexEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.8

    \brief A Private d_ptr of QtComplexEditFactory.

    \sa QtComplexEditFactory
*/
class QtComplexEditFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtComplexEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtComplexEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QtComplex& value);
    void slotRangeChanged(QtProperty *property, const QtComplex& min, const QtComplex& max);
    void slotPrecisionChanged(QtProperty *property, int prec);
    void slotReadOnlyChanged(QtProperty *property, bool readOnly);
    void slotSetValue(const QtComplex& value);
    void slotSetScale(int scaleSelection);
    void slotSetPkAvg(int slotSetPkAvg);
    void slotSetFormat(int formatSelection);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtComplexEditFactoryPrivate::slotPropertyChanged(QtProperty *property, const QtComplex& value)
{
    QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtComplexEdit *editor : it.value()) {
        editor->setScale(manager->scale(property));
        editor->setFormat(manager->format(property));
        if (!isclose(value, editor->value(), std::numeric_limits<double>::epsilon(), std::numeric_limits<double>::min())){
            editor->blockSignals(true);
            editor->setValue(value);
            editor->blockSignals(false);
        }
    }
    QList<QComboBox *> unitEditors = m_createdUnitAttributeEditors[property];
    for (unsigned short index = 0; index < unitEditors.size(); ++index ){
        updateUnit(manager, property, unitEditors[index]);
    }
    QList<QComboBox *> formatEditors = m_createdFormatAttributeEditors[property];
    for (unsigned short index = 0; index < formatEditors.size(); ++index ){
        updateFormat(manager, property, formatEditors[index]);
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtComplexEditFactoryPrivate::slotRangeChanged(QtProperty *property,
                                                   const QtComplex& min, const QtComplex& max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtComplexEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min.real(), max.real());
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtComplexEditFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QtComplexEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtComplexEditFactoryPrivate::slotPrecisionChanged(QtProperty *property, int prec)
{
    if (!m_createdEditors.contains(property))
        return;

    QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    QList<QtComplexEdit *> editors = m_createdEditors[property];
    QListIterator<QtComplexEdit *> itEditor(editors);
    while (itEditor.hasNext()) {
        QtComplexEdit *editor = itEditor.next();
        editor->blockSignals(true);
        editor->setPrecision(prec);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtComplexEditFactoryPrivate::slotSetValue(const QtComplex& value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtComplexEdit *, QtProperty *>::ConstIterator itcend = m_editorToProperty.constEnd();
    for (QMap<QtComplexEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
    }
}

void QtComplexEditFactoryPrivate::slotSetScale(int scaleSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_unitAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_unitAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setScale(property, Config::Scale(scaleSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtComplexEditFactoryPrivate::slotSetPkAvg(int pkAvgSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_pkAvgAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_pkAvgAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setPkAvg(property, Config::PkAvg(pkAvgSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtComplexEditFactoryPrivate::slotSetFormat(int formatSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_formatAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_formatAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setFormat(property, Config::Format(formatSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtComplexEditFactoryPrivate::slotSetMinimum(double minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, minVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtComplexEditFactoryPrivate::slotSetMaximum(double maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, maxVal);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtComplexEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}
/*! \class QtComplexEditFactory

 \brief The QtComplexEditFactory class provides QtComplexEdit
 widgets for properties created by QtDoublePropertyManager objects.

 \sa QtAbstractEditorFactory, QtDoublePropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtComplexEditFactory::QtComplexEditFactory(QObject *parent)
: QtAbstractEditorFactory<QtComplexPropertyManager>(parent), d_ptr(new QtComplexEditFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtComplexEditFactory::~QtComplexEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtComplexEditFactory::connectPropertyManager(QtComplexPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*, const QtComplex&)),
            this, SLOT(slotPropertyChanged(QtProperty*, const QtComplex&)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*, const QtComplex&, const QtComplex&)),
            this, SLOT(slotRangeChanged(QtProperty*, const QtComplex&, const QtComplex&)));
    connect(manager, SIGNAL(precisionChanged(QtProperty*, int)),
            this, SLOT(slotPrecisionChanged(QtProperty*, int)));
    connect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
            this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtComplexEditFactory::createEditor(QtComplexPropertyManager *manager,
                                              QtProperty *property, QWidget *parent)
{
    QtComplexEdit *editor = d_ptr->createEditor(property, parent);
    //editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property).real(), manager->maximum(property).real());
    editor->setPrecision(manager->precision(property));
    editor->setValue(manager->value(property));
    editor->setFormat(manager->format(property));
    editor->setScale(manager->scale(property));
    //editor->setKeyboardTracking(false);
    editor->setReadOnly(manager->isReadOnly(property));

    connect(editor, SIGNAL(valueChanged(const QtComplex&)), this, SLOT(slotSetValue(const QtComplex&)));
    connect(editor, SIGNAL(destroyed(QObject *)),
            this, SLOT(slotEditorDestroyed(QObject *)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtComplexEditFactory::createAttributeEditor(QtComplexPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::UNIT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::UNIT))
            return nullptr;
        QComboBox *editor = d_ptr->createUnitAttributeEditor(property, parent);
        updateUnit(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetScale(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotUnitAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::FORMAT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::FORMAT))
            return nullptr;
        QComboBox *editor = d_ptr->createFormatAttributeEditor(property, parent);
        updateFormat(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetFormat(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotFormatAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtComplexEditFactory::disconnectPropertyManager(QtComplexPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*, const QtComplex&)),
               this, SLOT(slotPropertyChanged(QtProperty*, const QtComplex&)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*, const QtComplex&, const QtComplex&)),
               this, SLOT(slotRangeChanged(QtProperty*, const QtComplex&, const QtComplex&)));
    disconnect(manager, SIGNAL(precisionChanged(QtProperty*, int)),
               this, SLOT(slotPrecisionChanged(QtProperty*, int)));
    disconnect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
               this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

// QtQuaternionEditFactory

/*!
    \class QtQuaternionEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.8

    \brief A Private d_ptr of QtQuaternionEditFactory.

    \sa QtQuaternionEditFactory
*/
class QtQuaternionEditFactoryPrivate : public EditorFactoryPrivate<QtQuaternionEdit>
{
    QtQuaternionEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtQuaternionEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QtQuaternion& value);
    void slotRangeChanged(QtProperty *property, const QtQuaternion& min, const QtQuaternion& max);
    void slotPrecisionChanged(QtProperty *property, int prec);
    void slotReadOnlyChanged(QtProperty *property, bool readOnly);
    void slotSetValue(const QtQuaternion& value);
    void slotSetScale(int scaleSelection);
    void slotSetPkAvg(int slotSetPkAvg);
    void slotSetFormat(int formatSelection);
    void slotSetMinimum(double minVal);
    void slotSetMaximum(double maxVal);
    void slotSetCheck(bool check);
};

void QtQuaternionEditFactoryPrivate::slotPropertyChanged(QtProperty *property, const QtQuaternion& value)
{
    QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    for (QtQuaternionEdit *editor : it.value()) {
        editor->setScale(manager->scale(property));
        editor->setFormat(manager->format(property));
        if (!isclose(value, editor->value(), QtQuaternion(std::numeric_limits<double>::epsilon()), QtQuaternion(std::numeric_limits<double>::min()))){
            editor->blockSignals(true);
            editor->setValue(value);
            editor->blockSignals(false);
        }
    }
    QList<QComboBox *> unitEditors = m_createdUnitAttributeEditors[property];
    for (unsigned short index = 0; index < unitEditors.size(); ++index ){
        updateUnit(manager, property, unitEditors[index]);
    }
    QList<QComboBox *> formatEditors = m_createdFormatAttributeEditors[property];
    for (unsigned short index = 0; index < formatEditors.size(); ++index ){
        updateFormat(manager, property, formatEditors[index]);
    }
    QList<QtDoubleEdit *> minimumEditors = m_createdMinimumAttributeEditors[property];
    for (unsigned short index = 0; index < minimumEditors.size(); ++index ){
        updateMinimum(manager, property, minimumEditors[index]);
    }
    QList<QtDoubleEdit *> maximumEditors = m_createdMaximumAttributeEditors[property];
    for (unsigned short index = 0; index < maximumEditors.size(); ++index ){
        updateMaximum(manager, property, maximumEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtQuaternionEditFactoryPrivate::slotRangeChanged(QtProperty *property,
                                                   const QtQuaternion& min, const QtQuaternion& max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;

    QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QtQuaternionEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setRange(min.length(), max.length());
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtQuaternionEditFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QtQuaternionEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtQuaternionEditFactoryPrivate::slotPrecisionChanged(QtProperty *property, int prec)
{
    if (!m_createdEditors.contains(property))
        return;

    QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    QList<QtQuaternionEdit *> editors = m_createdEditors[property];
    QListIterator<QtQuaternionEdit *> itEditor(editors);
    while (itEditor.hasNext()) {
        QtQuaternionEdit *editor = itEditor.next();
        editor->blockSignals(true);
        editor->setPrecision(prec);
        editor->setValue(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtQuaternionEditFactoryPrivate::slotSetValue(const QtQuaternion& value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtQuaternionEdit *, QtProperty *>::ConstIterator itcend = m_editorToProperty.constEnd();
    for (QMap<QtQuaternionEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
    }
}

void QtQuaternionEditFactoryPrivate::slotSetScale(int scaleSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_unitAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_unitAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setScale(property, Config::Scale(scaleSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtQuaternionEditFactoryPrivate::slotSetPkAvg(int pkAvgSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_pkAvgAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_pkAvgAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setPkAvg(property, Config::PkAvg(pkAvgSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtQuaternionEditFactoryPrivate::slotSetFormat(int formatSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_formatAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_formatAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setFormat(property, Config::Format(formatSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtQuaternionEditFactoryPrivate::slotSetMinimum(double minVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_minimumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_minimumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMinimum(property, {float(minVal)});
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtQuaternionEditFactoryPrivate::slotSetMaximum(double maxVal)
{
    QObject *object = q_ptr->sender();
    const QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itcend = m_maximumAttributeEditorToProperty.constEnd();
    for (QMap<QtDoubleEdit *, QtProperty *>::ConstIterator itEditor = m_maximumAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setMaximum(property, {float(maxVal)});
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtQuaternionEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtQuaternionPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}
/*! \class QtQuaternionEditFactory

 \brief The QtQuaternionEditFactory class provides QtQuaternionEdit
 widgets for properties created by QtDoublePropertyManager objects.

 \sa QtAbstractEditorFactory, QtDoublePropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtQuaternionEditFactory::QtQuaternionEditFactory(QObject *parent)
        : QtAbstractEditorFactory<QtQuaternionPropertyManager>(parent), d_ptr(new QtQuaternionEditFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtQuaternionEditFactory::~QtQuaternionEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtQuaternionEditFactory::connectPropertyManager(QtQuaternionPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*, const QtQuaternion&)),
            this, SLOT(slotPropertyChanged(QtProperty*, const QtQuaternion&)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*, const QtQuaternion&, const QtQuaternion&)),
            this, SLOT(slotRangeChanged(QtProperty*, const QtQuaternion&, const QtQuaternion&)));
    connect(manager, SIGNAL(precisionChanged(QtProperty*, int)),
            this, SLOT(slotPrecisionChanged(QtProperty*, int)));
    connect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
            this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtQuaternionEditFactory::createEditor(QtQuaternionPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    QtQuaternionEdit *editor = d_ptr->createEditor(property, parent);
    //editor->setSingleStep(manager->singleStep(property));
    editor->setRange(manager->minimum(property).length(), manager->maximum(property).length());
    editor->setPrecision(manager->precision(property));
    editor->setValue(manager->value(property));
    editor->setFormat(manager->format(property));
    editor->setScale(manager->scale(property));
    editor->setPolarized(manager->polarized(property));
    //editor->setKeyboardTracking(false);
    editor->setReadOnly(manager->isReadOnly(property));

    connect(editor, SIGNAL(valueChanged(const QtQuaternion&)), this, SLOT(slotSetValue(const QtQuaternion&)));
    connect(editor, SIGNAL(destroyed(QObject *)),
            this, SLOT(slotEditorDestroyed(QObject *)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtQuaternionEditFactory::createAttributeEditor(QtQuaternionPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::UNIT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::UNIT))
            return nullptr;
        QComboBox *editor = d_ptr->createUnitAttributeEditor(property, parent);
        updateUnit(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetScale(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotUnitAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::FORMAT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::FORMAT))
            return nullptr;
        QComboBox *editor = d_ptr->createFormatAttributeEditor(property, parent);
        updateFormat(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetFormat(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotFormatAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MINIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MINIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMinimumAttributeEditor(property, parent);
        updateMinimum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMinimum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMinimumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::MAXIMUM)
    {
        if (!manager->attributeEditable(Config::BrowserCol::MAXIMUM))
            return nullptr;
        QtDoubleEdit *editor = d_ptr->createMaximumAttributeEditor(property, parent);
        updateMaximum(manager, property, editor);

        connect(editor, SIGNAL(valueChanged(double)), this, SLOT(slotSetMaximum(double)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotMaximumAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtQuaternionEditFactory::disconnectPropertyManager(QtQuaternionPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*, const QtQuaternion&)),
               this, SLOT(slotPropertyChanged(QtProperty*, const QtQuaternion&)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*, const QtQuaternion&, const QtQuaternion&)),
               this, SLOT(slotRangeChanged(QtProperty*, const QtQuaternion&, const QtQuaternion&)));
    disconnect(manager, SIGNAL(precisionChanged(QtProperty*, int)),
               this, SLOT(slotPrecisionChanged(QtProperty*, int)));
    disconnect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
               this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

// QtArrayEditFactory

/*!
    \class QtVectorComplexEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.8

    \brief A Private d_ptr of QtVectorComplexEditFactory.

    \sa QtVectorComplexEditFactory
*/
class QtVectorComplexEditFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtVectorComplexEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtVectorComplexEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QVector<QtComplex>& value);
    void slotSetScale(int scaleSelection);
    void slotSetPkAvg(int formatSelection);
    void slotSetFormat(int formatSelection);
    void slotSetCheck(bool check);
    QtComplexEditFactory* m_subFactory;
};

void QtVectorComplexEditFactoryPrivate::slotPropertyChanged(QtProperty *property, const QVector<QtComplex>& value)
{
    QtVectorComplexPropertyManager* manager = q_ptr->propertyManager(property);
    for(int index=0; index < property->subProperties().size(); ++index){
        QtProperty* subProperty = property->subProperties()[index];
        m_subFactory->d_ptr->slotPropertyChanged(subProperty, value[index]);
    }
    QList<QComboBox *> unitEditors = m_createdUnitAttributeEditors[property];
    for (unsigned short index = 0; index < unitEditors.size(); ++index ){
        updateUnit(manager, property, unitEditors[index]);
    }
    QList<QComboBox *> formatEditors = m_createdFormatAttributeEditors[property];
    for (unsigned short index = 0; index < formatEditors.size(); ++index ){
        updateFormat(manager, property, formatEditors[index]);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtVectorComplexEditFactoryPrivate::slotSetScale(int scaleSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_unitAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_unitAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtVectorComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setScale(property, Config::Scale(scaleSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtVectorComplexEditFactoryPrivate::slotSetPkAvg(int pkAvgSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_pkAvgAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_pkAvgAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtVectorComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setPkAvg(property, Config::PkAvg(pkAvgSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtVectorComplexEditFactoryPrivate::slotSetFormat(int formatSelection)
{
    QObject *object = q_ptr->sender();
    const QMap<QComboBox *, QtProperty *>::ConstIterator itcend = m_formatAttributeEditorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_formatAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtVectorComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setFormat(property, Config::Format(formatSelection));
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

void QtVectorComplexEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtVectorComplexPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*! \class QtArrayEditFactory

 \brief The QtArrayEditFactory class provides QtComplexEdit
 widgets for properties created by QtDoublePropertyManager objects.

 \sa QtAbstractEditorFactory, QtDoublePropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtVectorComplexEditFactory::QtVectorComplexEditFactory(QObject *parent)
: QtAbstractEditorFactory<QtVectorComplexPropertyManager>(parent), d_ptr(new QtVectorComplexEditFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtVectorComplexEditFactory::~QtVectorComplexEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

QtComplexEditFactory* QtVectorComplexEditFactory::subFactory() const
{
    return d_ptr->m_subFactory;
}

void QtVectorComplexEditFactory::setSubFactory(QtComplexEditFactory* subFactory)
{
    d_ptr->m_subFactory = subFactory;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtVectorComplexEditFactory::connectPropertyManager(QtVectorComplexPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtVectorComplexEditFactory::createEditor(QtVectorComplexPropertyManager *manager,
                                                  QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtVectorComplexEditFactory::createAttributeEditor(QtVectorComplexPropertyManager *manager,
                                                           QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::UNIT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::UNIT))
            return nullptr;
        QComboBox *editor = d_ptr->createUnitAttributeEditor(property, parent);
        updateUnit(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetScale(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotUnitAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::PKAVG)
    {
        if (!manager->attributeEditable(Config::BrowserCol::PKAVG))
            return nullptr;
        QComboBox *editor = d_ptr->createPkAvgAttributeEditor(property, parent);
        updatePkAvg(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetPkAvg(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotPkAvgAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::FORMAT)
    {
        if (!manager->attributeEditable(Config::BrowserCol::FORMAT))
            return nullptr;
        QComboBox *editor = d_ptr->createFormatAttributeEditor(property, parent);
        updateFormat(manager, property, editor);

        connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetFormat(int)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotFormatAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    else if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtVectorComplexEditFactory::disconnectPropertyManager(QtVectorComplexPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtLineEditFactory

/*!
    \class QtLineEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtLineEditFactory.

    \sa QtLineEditFactory
*/
class QtLineEditFactoryPrivate : public EditorFactoryPrivate<QLineEdit>
{
    QtLineEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtLineEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QString &value);
    void slotRegExpChanged(QtProperty *property, const QRegularExpression &regExp);
    void slotSetValue(const QString &value);
    void slotReadOnlyChanged(QtProperty *, bool);
    void slotSetCheck(bool check);
};

void QtLineEditFactoryPrivate::slotPropertyChanged(QtProperty *property,
                const QString &value)
{
    QtStringPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QLineEdit *editor : it.value()) {
        if (editor->text() != value) {
            editor->blockSignals(true);
            editor->setText(value);
            editor->blockSignals(false);
        }
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtLineEditFactoryPrivate::slotRegExpChanged(QtProperty *property,
            const QRegularExpression &regExp)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    QtStringPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QLineEdit *editor : it.value()) {
        editor->blockSignals(true);
        const QValidator *oldValidator = editor->validator();
        QValidator *newValidator = nullptr;
        if (regExp.isValid()) {
            newValidator = new QRegularExpressionValidator(regExp, editor);
        }
        editor->setValidator(newValidator);
        if (oldValidator)
            delete oldValidator;
        editor->blockSignals(false);
    }
}

void QtLineEditFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtStringPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QLineEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtLineEditFactoryPrivate::slotSetValue(const QString &value)
{
    QObject *object = q_ptr->sender();
    const QMap<QLineEdit *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QLineEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtStringPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtLineEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtStringPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtLineEditFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtLineEditFactory class provides QLineEdit widgets for
    properties created by QtStringPropertyManager objects.

    \sa QtAbstractEditorFactory, QtStringPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtLineEditFactory::QtLineEditFactory(QObject *parent)
    : QtAbstractEditorFactory<QtStringPropertyManager>(parent), d_ptr(new QtLineEditFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtLineEditFactory::~QtLineEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtLineEditFactory::connectPropertyManager(QtStringPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QString)),
                this, SLOT(slotPropertyChanged(QtProperty*,QString)));
    connect(manager, SIGNAL(regExpChanged(QtProperty*,QRegularExpression)),
                this, SLOT(slotRegExpChanged(QtProperty*,QRegularExpression)));
    connect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
        this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtLineEditFactory::createEditor(QtStringPropertyManager *manager,
        QtProperty *property, QWidget *parent)
{

    QLineEdit *editor = d_ptr->createEditor(property, parent);
    editor->setReadOnly(manager->isReadOnly(property));
    QRegularExpression regExp = manager->regExp(property);
    if (regExp.isValid() && !regExp.pattern().isEmpty()) {
        QValidator *validator = new QRegularExpressionValidator(regExp, editor);
        editor->setValidator(validator);
    }
    editor->setText(manager->value(property));

    connect(editor, SIGNAL(textEdited(QString)),
                this, SLOT(slotSetValue(QString)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtLineEditFactory::createAttributeEditor(QtStringPropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtLineEditFactory::disconnectPropertyManager(QtStringPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QString)),
                this, SLOT(slotPropertyChanged(QtProperty*,QString)));
    disconnect(manager, SIGNAL(regExpChanged(QtProperty*,QRegularExpression)),
                this, SLOT(slotRegExpChanged(QtProperty*,QRegularExpression)));
    disconnect(manager, SIGNAL(readOnlyChanged(QtProperty*, bool)),
        this, SLOT(slotReadOnlyChanged(QtProperty*, bool)));

}

// QtDateEditFactory

/*!
    \class QtDateEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtDateEditFactory.

    \sa QtDateEditFactory
*/
class QtDateEditFactoryPrivate : public EditorFactoryPrivate<QDateEdit>
{
    QtDateEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtDateEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, QDate value);
    void slotRangeChanged(QtProperty *property, QDate min, QDate max);
    void slotSetValue(QDate value);
    void slotSetCheck(bool check);
};

void QtDateEditFactoryPrivate::slotPropertyChanged(QtProperty *property, QDate value)
{
    QtDatePropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;
    for (QDateEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setDate(value);
        editor->blockSignals(false);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtDateEditFactoryPrivate::slotRangeChanged(QtProperty *property, QDate min, QDate max)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    QtDatePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    for (QDateEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setDateRange(min, max);
        editor->setDate(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtDateEditFactoryPrivate::slotSetValue(QDate value)
{
    QObject *object = q_ptr->sender();
    const QMap<QDateEdit *, QtProperty *>::ConstIterator  ecend = m_editorToProperty.constEnd();
    for (QMap<QDateEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDatePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtDateEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDatePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtDateEditFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtDateEditFactory class provides QDateEdit widgets for
    properties created by QtDatePropertyManager objects.

    \sa QtAbstractEditorFactory, QtDatePropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtDateEditFactory::QtDateEditFactory(QObject *parent)
    : QtAbstractEditorFactory<QtDatePropertyManager>(parent), d_ptr(new QtDateEditFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtDateEditFactory::~QtDateEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtDateEditFactory::connectPropertyManager(QtDatePropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QDate)),
                this, SLOT(slotPropertyChanged(QtProperty*,QDate)));
    connect(manager, SIGNAL(rangeChanged(QtProperty*,QDate,QDate)),
                this, SLOT(slotRangeChanged(QtProperty*,QDate,QDate)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtDateEditFactory::createEditor(QtDatePropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QDateEdit *editor = d_ptr->createEditor(property, parent);
    editor->setDisplayFormat(QtPropertyBrowserUtils::dateFormat());
    editor->setCalendarPopup(true);
    editor->setDateRange(manager->minimum(property), manager->maximum(property));
    editor->setDate(manager->value(property));

    connect(editor, SIGNAL(dateChanged(QDate)),
                this, SLOT(slotSetValue(QDate)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtDateEditFactory::createAttributeEditor(QtDatePropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtDateEditFactory::disconnectPropertyManager(QtDatePropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QDate)),
                this, SLOT(slotPropertyChanged(QtProperty*,QDate)));
    disconnect(manager, SIGNAL(rangeChanged(QtProperty*,QDate,QDate)),
                this, SLOT(slotRangeChanged(QtProperty*,QDate,QDate)));
}

// QtTimeEditFactory

/*!
    \class QtTimeEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtTimeEditFactory.

    \sa QtTimeEditFactory
*/
class QtTimeEditFactoryPrivate : public EditorFactoryPrivate<QTimeEdit>
{
    QtTimeEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtTimeEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, QTime value);
    void slotSetValue(QTime value);
    void slotSetCheck(bool check);
};

void QtTimeEditFactoryPrivate::slotPropertyChanged(QtProperty *property, QTime value)
{
    QtTimePropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;
    for (QTimeEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setTime(value);
        editor->blockSignals(false);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtTimeEditFactoryPrivate::slotSetValue(QTime value)
{
    QObject *object = q_ptr->sender();
    const  QMap<QTimeEdit *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QTimeEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtTimePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtTimeEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtTimePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtTimeEditFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtTimeEditFactory class provides QTimeEdit widgets for
    properties created by QtTimePropertyManager objects.

    \sa QtAbstractEditorFactory, QtTimePropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtTimeEditFactory::QtTimeEditFactory(QObject *parent)
    : QtAbstractEditorFactory<QtTimePropertyManager>(parent), d_ptr(new QtTimeEditFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtTimeEditFactory::~QtTimeEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtTimeEditFactory::connectPropertyManager(QtTimePropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QTime)),
                this, SLOT(slotPropertyChanged(QtProperty*,QTime)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtTimeEditFactory::createEditor(QtTimePropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QTimeEdit *editor = d_ptr->createEditor(property, parent);
    editor->setDisplayFormat(QtPropertyBrowserUtils::timeFormat());
    editor->setTime(manager->value(property));

    connect(editor, SIGNAL(timeChanged(QTime)),
                this, SLOT(slotSetValue(QTime)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtTimeEditFactory::createAttributeEditor(QtTimePropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtTimeEditFactory::disconnectPropertyManager(QtTimePropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QTime)),
                this, SLOT(slotPropertyChanged(QtProperty*,QTime)));
}

// QtDateTimeEditFactory

/*!
    \class QtDateTimeEditFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtDateTimeEditFactory.

    \sa QtDateTimeEditFactory
*/
class QtDateTimeEditFactoryPrivate : public EditorFactoryPrivate<QDateTimeEdit>
{
    QtDateTimeEditFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtDateTimeEditFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QDateTime &value);
    void slotSetValue(const QDateTime &value);
    void slotSetCheck(bool check);

};

void QtDateTimeEditFactoryPrivate::slotPropertyChanged(QtProperty *property,
            const QDateTime &value)
{
    QtDateTimePropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QDateTimeEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setDateTime(value);
        editor->blockSignals(false);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtDateTimeEditFactoryPrivate::slotSetValue(const QDateTime &value)
{
    QObject *object = q_ptr->sender();
    const  QMap<QDateTimeEdit *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QDateTimeEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend;  ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDateTimePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtDateTimeEditFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtDateTimePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtDateTimeEditFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtDateTimeEditFactory class provides QDateTimeEdit
    widgets for properties created by QtDateTimePropertyManager objects.

    \sa QtAbstractEditorFactory, QtDateTimePropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtDateTimeEditFactory::QtDateTimeEditFactory(QObject *parent)
    : QtAbstractEditorFactory<QtDateTimePropertyManager>(parent), d_ptr(new QtDateTimeEditFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtDateTimeEditFactory::~QtDateTimeEditFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtDateTimeEditFactory::connectPropertyManager(QtDateTimePropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QDateTime)),
                this, SLOT(slotPropertyChanged(QtProperty*,QDateTime)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtDateTimeEditFactory::createEditor(QtDateTimePropertyManager *manager,
        QtProperty *property, QWidget *parent)
{
    QDateTimeEdit *editor =  d_ptr->createEditor(property, parent);
    editor->setDisplayFormat(QtPropertyBrowserUtils::dateTimeFormat());
    editor->setDateTime(manager->value(property));

    connect(editor, SIGNAL(dateTimeChanged(QDateTime)),
                this, SLOT(slotSetValue(QDateTime)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
    \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtDateTimeEditFactory::createAttributeEditor(QtDateTimePropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtDateTimeEditFactory::disconnectPropertyManager(QtDateTimePropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QDateTime)),
                this, SLOT(slotPropertyChanged(QtProperty*,QDateTime)));
}

// QtKeySequenceEditorFactory

/*!
    \class QtKeySequenceEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtKeySequenceEditorFactory.

    \sa QtKeySequenceEditorFactory
*/
class QtKeySequenceEditorFactoryPrivate : public EditorFactoryPrivate<QKeySequenceEdit>
{
    QtKeySequenceEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtKeySequenceEditorFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QKeySequence &value);
    void slotSetValue(const QKeySequence &value);
    void slotSetCheck(bool check);
};

void QtKeySequenceEditorFactoryPrivate::slotPropertyChanged(QtProperty *property,
            const QKeySequence &value)
{
    QtKeySequencePropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QKeySequenceEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setKeySequence(value);
        editor->blockSignals(false);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtKeySequenceEditorFactoryPrivate::slotSetValue(const QKeySequence &value)
{
    QObject *object = q_ptr->sender();
    const  QMap<QKeySequenceEdit *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QKeySequenceEdit *, QtProperty *>::ConstIterator itEditor =  m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtKeySequencePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtKeySequenceEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtKeySequencePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtKeySequenceEditorFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtKeySequenceEditorFactory class provides editor
    widgets for properties created by QtKeySequencePropertyManager objects.

    \sa QtAbstractEditorFactory
*/

/*!
    Creates a factory with the given \a parent.
*/
QtKeySequenceEditorFactory::QtKeySequenceEditorFactory(QObject *parent)
    : QtAbstractEditorFactory<QtKeySequencePropertyManager>(parent), d_ptr(new QtKeySequenceEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtKeySequenceEditorFactory::~QtKeySequenceEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtKeySequenceEditorFactory::connectPropertyManager(QtKeySequencePropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QKeySequence)),
                this, SLOT(slotPropertyChanged(QtProperty*,QKeySequence)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtKeySequenceEditorFactory::createEditor(QtKeySequencePropertyManager *manager,
        QtProperty *property, QWidget *parent)
{
    QKeySequenceEdit *editor = d_ptr->createEditor(property, parent);
    editor->setKeySequence(manager->value(property));

    connect(editor, SIGNAL(keySequenceChanged(QKeySequence)),
                this, SLOT(slotSetValue(QKeySequence)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
    \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtKeySequenceEditorFactory::createAttributeEditor(QtKeySequencePropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtKeySequenceEditorFactory::disconnectPropertyManager(QtKeySequencePropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QKeySequence)),
                this, SLOT(slotPropertyChanged(QtProperty*,QKeySequence)));
}

// QtCharEditorFactory

/*!
    \class QtCharEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtCharEditorFactory.

    \sa QtCharEditorFactory
*/
class QtCharEditorFactoryPrivate : public EditorFactoryPrivate<QtCharEdit>
{
    QtCharEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtCharEditorFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QChar &value);
    void slotSetValue(const QChar &value);
    void slotSetCheck(bool check);

};

void QtCharEditorFactoryPrivate::slotPropertyChanged(QtProperty *property,
            const QChar &value)
{
    QtCharPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QtCharEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setValue(value);
        editor->blockSignals(false);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtCharEditorFactoryPrivate::slotSetValue(const QChar &value)
{
    QObject *object = q_ptr->sender();
    const QMap<QtCharEdit *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QtCharEdit *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend;  ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtCharPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtCharEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtCharPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtCharEditorFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtCharEditorFactory class provides editor
    widgets for properties created by QtCharPropertyManager objects.

    \sa QtAbstractEditorFactory
*/

/*!
    Creates a factory with the given \a parent.
*/
QtCharEditorFactory::QtCharEditorFactory(QObject *parent)
    : QtAbstractEditorFactory<QtCharPropertyManager>(parent), d_ptr(new QtCharEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtCharEditorFactory::~QtCharEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtCharEditorFactory::connectPropertyManager(QtCharPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QChar)),
                this, SLOT(slotPropertyChanged(QtProperty*,QChar)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtCharEditorFactory::createEditor(QtCharPropertyManager *manager,
        QtProperty *property, QWidget *parent)
{
    QtCharEdit *editor = d_ptr->createEditor(property, parent);
    editor->setValue(manager->value(property));

    connect(editor, SIGNAL(valueChanged(QChar)),
                this, SLOT(slotSetValue(QChar)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
    \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtCharEditorFactory::createAttributeEditor(QtCharPropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtCharEditorFactory::disconnectPropertyManager(QtCharPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QChar)),
                this, SLOT(slotPropertyChanged(QtProperty*,QChar)));
}

// QtLocaleEditorFactory

/*!
    \class QtLocaleEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtLocaleEditorFactory.

    \sa QtLocaleEditorFactory
*/
class QtLocaleEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtLocaleEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtLocaleEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtLocaleEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtLocalePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtLocaleEditorFactory

 \brief The QtLocaleEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtLocaleEditorFactory::QtLocaleEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtLocalePropertyManager>(parent), d_ptr(new QtLocaleEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtLocaleEditorFactory::~QtLocaleEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtLocaleEditorFactory::connectPropertyManager(QtLocalePropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtLocaleEditorFactory::createEditor(QtLocalePropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtLocaleEditorFactory::createAttributeEditor(QtLocalePropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtLocaleEditorFactory::disconnectPropertyManager(QtLocalePropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtPointEditorFactory

/*!
    \class QtPointEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtPointEditorFactory.

    \sa QtPointEditorFactory
*/
class QtPointEditorFactoryPrivate : public EditorFactoryPrivate<QtIntEdit>
{
    QtPointEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtPointEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtPointEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtPointPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtPointEditorFactory

 \brief The QtPointEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtPointEditorFactory::QtPointEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtPointPropertyManager>(parent), d_ptr(new QtPointEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtPointEditorFactory::~QtPointEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtPointEditorFactory::connectPropertyManager(QtPointPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtPointEditorFactory::createEditor(QtPointPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtPointEditorFactory::createAttributeEditor(QtPointPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtPointEditorFactory::disconnectPropertyManager(QtPointPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtPointFEditorFactory

/*!
    \class QtPointFEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtPointFEditorFactory.

    \sa QtPointFEditorFactory
*/
class QtPointFEditorFactoryPrivate : public EditorFactoryPrivate<QtDoubleEdit>
{
    QtPointFEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtPointFEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtPointFEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtPointFPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtPointFEditorFactory

 \brief The QtPointFEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtPointFEditorFactory::QtPointFEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtPointFPropertyManager>(parent), d_ptr(new QtPointFEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtPointFEditorFactory::~QtPointFEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtPointFEditorFactory::connectPropertyManager(QtPointFPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtPointFEditorFactory::createEditor(QtPointFPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtPointFEditorFactory::createAttributeEditor(QtPointFPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtPointFEditorFactory::disconnectPropertyManager(QtPointFPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtSizeEditorFactory

/*!
    \class QtSizeEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtSizeEditorFactory.

    \sa QtSizeEditorFactory
*/
class QtSizeEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtSizeEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtSizeEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtSizeEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtSizePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtSizeEditorFactory

 \brief The QtSizeEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtSizeEditorFactory::QtSizeEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtSizePropertyManager>(parent), d_ptr(new QtSizeEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtSizeEditorFactory::~QtSizeEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtSizeEditorFactory::connectPropertyManager(QtSizePropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSizeEditorFactory::createEditor(QtSizePropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSizeEditorFactory::createAttributeEditor(QtSizePropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtSizeEditorFactory::disconnectPropertyManager(QtSizePropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtSizeFEditorFactory

/*!
    \class QtSizeFEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtSizeFEditorFactory.

    \sa QtSizeFEditorFactory
*/
class QtSizeFEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtSizeFEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtSizeFEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtSizeFEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtSizeFPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtSizeFEditorFactory

 \brief The QtSizeFEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtSizeFEditorFactory::QtSizeFEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtSizeFPropertyManager>(parent), d_ptr(new QtSizeFEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtSizeFEditorFactory::~QtSizeFEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtSizeFEditorFactory::connectPropertyManager(QtSizeFPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSizeFEditorFactory::createEditor(QtSizeFPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSizeFEditorFactory::createAttributeEditor(QtSizeFPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtSizeFEditorFactory::disconnectPropertyManager(QtSizeFPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtRectEditorFactory

/*!
    \class QtRectEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtRectEditorFactory.

    \sa QtRectEditorFactory
*/
class QtRectEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtRectEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtRectEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtRectEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtRectPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtRectEditorFactory

 \brief The QtRectEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtRectEditorFactory::QtRectEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtRectPropertyManager>(parent), d_ptr(new QtRectEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtRectEditorFactory::~QtRectEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtRectEditorFactory::connectPropertyManager(QtRectPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtRectEditorFactory::createEditor(QtRectPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtRectEditorFactory::createAttributeEditor(QtRectPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtRectEditorFactory::disconnectPropertyManager(QtRectPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtRectFEditorFactory

/*!
    \class QtRectFEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtRectFEditorFactory.

    \sa QtRectFEditorFactory
*/
class QtRectFEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtRectFEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtRectFEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtRectFEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtRectFPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtRectFEditorFactory

 \brief The QtRectFEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtRectFEditorFactory::QtRectFEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtRectFPropertyManager>(parent), d_ptr(new QtRectFEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtRectFEditorFactory::~QtRectFEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtRectFEditorFactory::connectPropertyManager(QtRectFPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtRectFEditorFactory::createEditor(QtRectFPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtRectFEditorFactory::createAttributeEditor(QtRectFPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtRectFEditorFactory::disconnectPropertyManager(QtRectFPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtEnumEditorFactory

/*!
    \class QtEnumEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtEnumEditorFactory.

    \sa QtEnumEditorFactory
*/
class QtEnumEditorFactoryPrivate : public EditorFactoryPrivate<QComboBox>
{
    QtEnumEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtEnumEditorFactory)
public:

    void slotPropertyChanged(QtProperty *property, int value);
    void slotEnumNamesChanged(QtProperty *property, const QStringList &);
    void slotEnumIconsChanged(QtProperty *property, const QMap<int, QIcon> &);
    void slotSetValue(int value);
    void slotSetCheck(bool check);
};

void QtEnumEditorFactoryPrivate::slotPropertyChanged(QtProperty *property, int value)
{
    QtEnumPropertyManager *manager = q_ptr->propertyManager(property);
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QComboBox *editor : it.value()) {
        editor->blockSignals(true);
        editor->setCurrentIndex(value);
        editor->blockSignals(false);
    }
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtEnumEditorFactoryPrivate::slotEnumNamesChanged(QtProperty *property,
                const QStringList &enumNames)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    QtEnumPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    QMap<int, QIcon> enumIcons = manager->enumIcons(property);

    for (QComboBox *editor : it.value()) {
        editor->blockSignals(true);
        editor->clear();
        editor->addItems(enumNames);
        const int nameCount = enumNames.size();
        for (int i = 0; i < nameCount; i++)
            editor->setItemIcon(i, enumIcons.value(i));
        editor->setCurrentIndex(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtEnumEditorFactoryPrivate::slotEnumIconsChanged(QtProperty *property,
                const QMap<int, QIcon> &enumIcons)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    QtEnumPropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;

    const QStringList enumNames = manager->enumNames(property);
    for (QComboBox *editor : it.value()) {
        editor->blockSignals(true);
        const int nameCount = enumNames.size();
        for (int i = 0; i < nameCount; i++)
            editor->setItemIcon(i, enumIcons.value(i));
        editor->setCurrentIndex(manager->value(property));
        editor->blockSignals(false);
    }
}

void QtEnumEditorFactoryPrivate::slotSetValue(int value)
{
    QObject *object = q_ptr->sender();
    const  QMap<QComboBox *, QtProperty *>::ConstIterator ecend = m_editorToProperty.constEnd();
    for (QMap<QComboBox *, QtProperty *>::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtEnumPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtEnumEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtEnumPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtEnumEditorFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtEnumEditorFactory class provides QComboBox widgets for
    properties created by QtEnumPropertyManager objects.

    \sa QtAbstractEditorFactory, QtEnumPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtEnumEditorFactory::QtEnumEditorFactory(QObject *parent)
    : QtAbstractEditorFactory<QtEnumPropertyManager>(parent), d_ptr(new QtEnumEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtEnumEditorFactory::~QtEnumEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtEnumEditorFactory::connectPropertyManager(QtEnumPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    connect(manager, SIGNAL(enumNamesChanged(QtProperty*,QStringList)),
                this, SLOT(slotEnumNamesChanged(QtProperty*,QStringList)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtEnumEditorFactory::createEditor(QtEnumPropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QComboBox *editor = d_ptr->createEditor(property, parent);
    editor->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLengthWithIcon);
    editor->setMinimumContentsLength(1);
    editor->view()->setTextElideMode(Qt::ElideRight);
    QStringList enumNames = manager->enumNames(property);
    editor->addItems(enumNames);
    QMap<int, QIcon> enumIcons = manager->enumIcons(property);
    const int enumNamesCount = enumNames.size();
    for (int i = 0; i < enumNamesCount; i++)
        editor->setItemIcon(i, enumIcons.value(i));
    editor->setCurrentIndex(manager->value(property));

    connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(slotSetValue(int)));
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtEnumEditorFactory::createAttributeEditor(QtEnumPropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtEnumEditorFactory::disconnectPropertyManager(QtEnumPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotPropertyChanged(QtProperty*,int)));
    disconnect(manager, SIGNAL(enumNamesChanged(QtProperty*,QStringList)),
                this, SLOT(slotEnumNamesChanged(QtProperty*,QStringList)));
}

// QtFlagEditorFactory

/*!
    \class QtFlagEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtFlagEditorFactory.

    \sa QtFlagEditorFactory
*/
class QtFlagEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtFlagEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtFlagEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtFlagEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtFlagPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtFlagEditorFactory

 \brief The QtFlagEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtFlagEditorFactory::QtFlagEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtFlagPropertyManager>(parent), d_ptr(new QtFlagEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtFlagEditorFactory::~QtFlagEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtFlagEditorFactory::connectPropertyManager(QtFlagPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtFlagEditorFactory::createEditor(QtFlagPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtFlagEditorFactory::createAttributeEditor(QtFlagPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtFlagEditorFactory::disconnectPropertyManager(QtFlagPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtSizePolicyEditorFactory

/*!
    \class QtSizePolicyEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtSizePolicyEditorFactory.

    \sa QtSizePolicyEditorFactory
*/
class QtSizePolicyEditorFactoryPrivate : public EditorFactoryPrivate<QtComplexEdit>
{
    QtSizePolicyEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtSizePolicyEditorFactory)
public:
    void slotSetCheck(bool check);
};

void QtSizePolicyEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtSizePolicyPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            return;
        }
    }
}

/*! \class QtSizePolicyEditorFactory

 \brief The QtSizePolicyEditorFactory class provides QtComplexEdit
 widgets for properties created by QtAbstractPropertyManager objects.

 \sa QtAbstractEditorFactory, QtAbstractPropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtSizePolicyEditorFactory::QtSizePolicyEditorFactory(QObject *parent)
: QtAbstractEditorFactory<QtSizePolicyPropertyManager>(parent), d_ptr(new QtSizePolicyEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtSizePolicyEditorFactory::~QtSizePolicyEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_unitAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_pkAvgAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_formatAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_minimumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_maximumAttributeEditorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtSizePolicyEditorFactory::connectPropertyManager(QtSizePolicyPropertyManager *manager)
{

    Q_UNUSED(manager);
    return;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSizePolicyEditorFactory::createEditor(QtSizePolicyPropertyManager *manager,
                                            QtProperty *property, QWidget *parent)
{
    Q_UNUSED(manager);
    return new QLabel(property->valueText(), parent);
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtSizePolicyEditorFactory::createAttributeEditor(QtSizePolicyPropertyManager *manager,
                                                     QtProperty *property, QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtSizePolicyEditorFactory::disconnectPropertyManager(QtSizePolicyPropertyManager *manager)
{
    Q_UNUSED(manager);
    return;
}

// QtCursorEditorFactory

Q_GLOBAL_STATIC(QtCursorDatabase, cursorDatabase)

/*!
    \class QtCursorEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtCursorEditorFactory.

    \sa QtCursorEditorFactory
*/
class QtCursorEditorFactoryPrivate: public EditorFactoryPrivate<QComboBox>
{
    QtCursorEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtCursorEditorFactory)
public:
    QtCursorEditorFactoryPrivate();

    void slotPropertyChanged(QtProperty *property, const QCursor &cursor);
    void slotEnumChanged(QtProperty *property, int value);
    void slotEditorDestroyed(QObject *object);
    void slotSetCheck(bool check);

    QtEnumEditorFactory *m_enumEditorFactory;
    QtEnumPropertyManager *m_enumPropertyManager;

    QMap<QtProperty *, QtProperty *> m_propertyToEnum;
    QMap<QtProperty *, QtProperty *> m_enumToProperty;
    QMap<QtProperty *, QWidgetList > m_enumToEditors;
    QMap<QWidget *, QtProperty *> m_editorToEnum;
    bool m_updatingEnum;
};

QtCursorEditorFactoryPrivate::QtCursorEditorFactoryPrivate()
    : m_updatingEnum(false)
{

}

void QtCursorEditorFactoryPrivate::slotPropertyChanged(QtProperty *property, const QCursor &cursor)
{
    QtCursorPropertyManager *manager = q_ptr->propertyManager(property);
    // update enum property
    QtProperty *enumProp = m_propertyToEnum.value(property);
    if (!enumProp)
        return;

    m_updatingEnum = true;
    m_enumPropertyManager->setValue(enumProp, cursorDatabase()->cursorToValue(cursor));
    m_updatingEnum = false;

    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtCursorEditorFactoryPrivate::slotEnumChanged(QtProperty *property, int value)
{
    if (m_updatingEnum)
        return;
    // update cursor property
    QtProperty *prop = m_enumToProperty.value(property);
    if (!prop)
        return;
    QtCursorPropertyManager *cursorManager = q_ptr->propertyManager(prop);
    if (!cursorManager)
        return;
#ifndef QT_NO_CURSOR
    cursorManager->setValue(prop, QCursor(cursorDatabase()->valueToCursor(value)));
#endif
}

void QtCursorEditorFactoryPrivate::slotEditorDestroyed(QObject *object)
{
    // remove from m_editorToEnum map;
    // remove from m_enumToEditors map;
    // if m_enumToEditors doesn't contains more editors delete enum property;
    const  QMap<QWidget *, QtProperty *>::ConstIterator ecend = m_editorToEnum.constEnd();
    for (QMap<QWidget *, QtProperty *>::ConstIterator itEditor = m_editorToEnum.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QWidget *editor = itEditor.key();
            QtProperty *enumProp = itEditor.value();
            m_editorToEnum.remove(editor);
            m_enumToEditors[enumProp].removeAll(editor);
            if (m_enumToEditors[enumProp].isEmpty()) {
                m_enumToEditors.remove(enumProp);
                QtProperty *property = m_enumToProperty.value(enumProp);
                m_enumToProperty.remove(enumProp);
                m_propertyToEnum.remove(property);
                delete enumProp;
            }
            return;
        }
}

void QtCursorEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtCursorPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtCursorEditorFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtCursorEditorFactory class provides QComboBox widgets for
    properties created by QtCursorPropertyManager objects.

    \sa QtAbstractEditorFactory, QtCursorPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtCursorEditorFactory::QtCursorEditorFactory(QObject *parent)
    : QtAbstractEditorFactory<QtCursorPropertyManager>(parent), d_ptr(new QtCursorEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;

    d_ptr->m_enumEditorFactory = new QtEnumEditorFactory(this);
    d_ptr->m_enumPropertyManager = new QtEnumPropertyManager(this);
    connect(d_ptr->m_enumPropertyManager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(slotEnumChanged(QtProperty*,int)));
    d_ptr->m_enumEditorFactory->addPropertyManager(d_ptr->m_enumPropertyManager);
}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtCursorEditorFactory::~QtCursorEditorFactory()
{
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtCursorEditorFactory::connectPropertyManager(QtCursorPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QCursor)),
                this, SLOT(slotPropertyChanged(QtProperty*,QCursor)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtCursorEditorFactory::createEditor(QtCursorPropertyManager *manager, QtProperty *property,
        QWidget *parent)
{
    QtProperty *enumProp = nullptr;
    if (d_ptr->m_propertyToEnum.contains(property)) {
        enumProp = d_ptr->m_propertyToEnum[property];
    } else {
        enumProp = d_ptr->m_enumPropertyManager->addProperty(property->propertyName());
        d_ptr->m_enumPropertyManager->setEnumNames(enumProp, cursorDatabase()->cursorShapeNames());
        d_ptr->m_enumPropertyManager->setEnumIcons(enumProp, cursorDatabase()->cursorShapeIcons());
#ifndef QT_NO_CURSOR
        d_ptr->m_enumPropertyManager->setValue(enumProp, cursorDatabase()->cursorToValue(manager->value(property)));
#endif
        d_ptr->m_propertyToEnum[property] = enumProp;
        d_ptr->m_enumToProperty[enumProp] = property;
    }
    QtAbstractEditorFactoryBase *af = d_ptr->m_enumEditorFactory;
    QWidget *editor = af->createEditor(enumProp, parent);
    d_ptr->m_enumToEditors[enumProp].append(editor);
    d_ptr->m_editorToEnum[editor] = enumProp;
    connect(editor, SIGNAL(destroyed(QObject*)),
                this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
    \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtCursorEditorFactory::createAttributeEditor(QtCursorPropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtCursorEditorFactory::disconnectPropertyManager(QtCursorPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QCursor)),
                this, SLOT(slotPropertyChanged(QtProperty*,QCursor)));
}

// QtColorEditorFactoryPrivate

/*!
    \class QtColorEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtColorEditorFactory.

    \sa QtColorEditorFactory
*/
class QtColorEditorFactoryPrivate : public EditorFactoryPrivate<QtColorEditWidget>
{
    QtColorEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtColorEditorFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QColor &value);
    void slotSetValue(const QColor &value);
    void slotSetCheck(bool check);
};

void QtColorEditorFactoryPrivate::slotPropertyChanged(QtProperty *property,
                const QColor &value)
{
    QtColorPropertyManager *manager = q_ptr->propertyManager(property);
    const PropertyToEditorListMap::const_iterator it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QtColorEditWidget *e : it.value())
        e->setValue(value);
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtColorEditorFactoryPrivate::slotSetValue(const QColor &value)
{
    QObject *object = q_ptr->sender();
    const EditorToPropertyMap::ConstIterator ecend = m_editorToProperty.constEnd();
    for (EditorToPropertyMap::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtColorPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtColorEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtColorPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtColorEditorFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtColorEditorFactory class provides color editing  for
    properties created by QtColorPropertyManager objects.

    \sa QtAbstractEditorFactory, QtColorPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtColorEditorFactory::QtColorEditorFactory(QObject *parent) :
    QtAbstractEditorFactory<QtColorPropertyManager>(parent),
    d_ptr(new QtColorEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtColorEditorFactory::~QtColorEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtColorEditorFactory::connectPropertyManager(QtColorPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QColor)),
            this, SLOT(slotPropertyChanged(QtProperty*,QColor)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtColorEditorFactory::createEditor(QtColorPropertyManager *manager,
        QtProperty *property, QWidget *parent)
{
    QtColorEditWidget *editor = d_ptr->createEditor(property, parent);
    editor->setValue(manager->value(property));
    connect(editor, SIGNAL(valueChanged(QColor)), this, SLOT(slotSetValue(QColor)));
    connect(editor, SIGNAL(destroyed(QObject*)), this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtColorEditorFactory::createAttributeEditor(QtColorPropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtColorEditorFactory::disconnectPropertyManager(QtColorPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QColor)), this, SLOT(slotPropertyChanged(QtProperty*,QColor)));
}

// QtFontEditorFactoryPrivate

/*!
    \class QtFontEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtFontEditorFactory.

    \sa QtFontEditorFactory
*/
class QtFontEditorFactoryPrivate : public EditorFactoryPrivate<QtFontEditWidget>
{
    QtFontEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtFontEditorFactory)
public:

    void slotPropertyChanged(QtProperty *property, const QFont &value);
    void slotSetValue(const QFont &value);
    void slotSetCheck(bool check);
};

void QtFontEditorFactoryPrivate::slotPropertyChanged(QtProperty *property,
                const QFont &value)
{
    QtFontPropertyManager *manager = q_ptr->propertyManager(property);
    const PropertyToEditorListMap::const_iterator it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.constEnd())
        return;

    for (QtFontEditWidget *e : it.value())
        e->setValue(value);
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtFontEditorFactoryPrivate::slotSetValue(const QFont &value)
{
    QObject *object = q_ptr->sender();
    const EditorToPropertyMap::ConstIterator ecend = m_editorToProperty.constEnd();
    for (EditorToPropertyMap::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtFontPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtFontEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtFontPropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
    \class QtFontEditorFactory
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief The QtFontEditorFactory class provides font editing for
    properties created by QtFontPropertyManager objects.

    \sa QtAbstractEditorFactory, QtFontPropertyManager
*/

/*!
    Creates a factory with the given \a parent.
*/
QtFontEditorFactory::QtFontEditorFactory(QObject *parent) :
    QtAbstractEditorFactory<QtFontPropertyManager>(parent),
    d_ptr(new QtFontEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
    Destroys this factory, and all the widgets it has created.
*/
QtFontEditorFactory::~QtFontEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtFontEditorFactory::connectPropertyManager(QtFontPropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QFont)),
            this, SLOT(slotPropertyChanged(QtProperty*,QFont)));
}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
QWidget *QtFontEditorFactory::createEditor(QtFontPropertyManager *manager,
        QtProperty *property, QWidget *parent)
{
    QtFontEditWidget *editor = d_ptr->createEditor(property, parent);
    editor->setValue(manager->value(property));
    connect(editor, SIGNAL(valueChanged(QFont)), this, SLOT(slotSetValue(QFont)));
    connect(editor, SIGNAL(destroyed(QObject*)), this, SLOT(slotEditorDestroyed(QObject*)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtFontEditorFactory::createAttributeEditor(QtFontPropertyManager *manager, QtProperty *property,
                                                  QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
    \internal

    Reimplemented from the QtAbstractEditorFactory class.
*/
void QtFontEditorFactory::disconnectPropertyManager(QtFontPropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QFont)), this, SLOT(slotPropertyChanged(QtProperty*,QFont)));
}

// QtFileEditorFactoryPrivate

/*!
    \class QtFileEditorFactoryPrivate
    \internal
    \inmodule QtDesigner
    \since 4.4

    \brief A Private d_ptr of QtFileEditorFactory.

    \sa QtFileEditorFactory
*/
class QtFileEditorFactoryPrivate : public EditorFactoryPrivate<QtFileEdit>
{
    QtFileEditorFactory *q_ptr;
    Q_DECLARE_PUBLIC(QtFileEditorFactory)
public:
    void slotPropertyChanged(QtProperty *property, const QString &value);
    void slotSetValue(const QString &value);
    void slotFilterChanged(QtProperty *property, const QString &value);
    void slotReadOnlyChanged(QtProperty *property, const bool value);
    void slotSetCheck(bool check);
};

void QtFileEditorFactoryPrivate::slotPropertyChanged(QtProperty *property, const QString &value)
{
    QtFilePropertyManager *manager = q_ptr->propertyManager(property);
    const PropertyToEditorListMap::iterator it = m_createdEditors.find(property);
    if (it == m_createdEditors.end())
        return;

    for (QtFileEdit *e : it.value())
        e->setValue(value);
    QList<QtBoolEdit *> checkEditors = m_createdCheckAttributeEditors[property];
    for (unsigned short index = 0; index < checkEditors.size(); ++index ){
        updateCheck(manager, property, checkEditors[index]);
    }
}

void QtFileEditorFactoryPrivate::slotSetValue(const QString &value)
{
    QObject *object = q_ptr->sender();
    const EditorToPropertyMap::ConstIterator ecend = m_editorToProperty.constEnd();
    for (EditorToPropertyMap::ConstIterator itEditor = m_editorToProperty.constBegin(); itEditor != ecend; ++itEditor)
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtFilePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setValue(property, value);
            return;
        }
}

void QtFileEditorFactoryPrivate::slotFilterChanged(QtProperty *property, const QString &value)
{
    const auto it = m_createdEditors.find(property);
    if (it == m_createdEditors.end())
        return;
    QListIterator<QtFileEdit *> itEditor(it.value());

    while (itEditor.hasNext())
        itEditor.next()->setValue(value);
}

void QtFileEditorFactoryPrivate::slotReadOnlyChanged( QtProperty *property, bool readOnly)
{
    const auto it = m_createdEditors.constFind(property);
    if (it == m_createdEditors.cend())
        return;
    QtFilePropertyManager *manager = q_ptr->propertyManager(property);
    if (!manager)
        return;
    for (QtFileEdit *editor : it.value()) {
        editor->blockSignals(true);
        editor->setReadOnly(readOnly);
        editor->blockSignals(false);
    }
}

void QtFileEditorFactoryPrivate::slotSetCheck(bool check)
{
    QObject *object = q_ptr->sender();
    const QMap<QtBoolEdit *, QtProperty *>::ConstIterator itcend = m_checkAttributeEditorToProperty.constEnd();
    for (QMap<QtBoolEdit *, QtProperty *>::ConstIterator itEditor = m_checkAttributeEditorToProperty.constBegin(); itEditor != itcend; ++itEditor) {
        if (itEditor.key() == object) {
            QtProperty *property = itEditor.value();
            QtFilePropertyManager *manager = q_ptr->propertyManager(property);
            if (!manager)
                return;
            manager->setCheck(property, check);
            slotPropertyChanged(property, manager->value(property));
            return;
        }
    }
}

/*!
 \class QtFileEditorFactory

 \brief The QtFileEditorFactory class provides file editor selection for
 properties created by QtFilePropertyManager objects.

 \sa QtAbstractEditorFactory, QtFilePropertyManager
 */

/*!
 Creates a factory with the given \a parent.
 */
QtFileEditorFactory::QtFileEditorFactory(QObject *parent) :
QtAbstractEditorFactory<QtFilePropertyManager>(parent),
d_ptr(new QtFileEditorFactoryPrivate())
{
    d_ptr->q_ptr = this;
}

/*!
 Destroys this factory, and all the widgets it has created.
 */
QtFileEditorFactory::~QtFileEditorFactory()
{
    qDeleteAll(d_ptr->m_editorToProperty.keys());
    qDeleteAll(d_ptr->m_checkAttributeEditorToProperty.keys());
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtFileEditorFactory::connectPropertyManager(QtFilePropertyManager *manager)
{
    connect(manager, SIGNAL(valueChanged(QtProperty*,QString)),
            this, SLOT(slotPropertyChanged(QtProperty*,QString)));
    connect(manager, SIGNAL(filterChanged(QtProperty*,QString)),
            this, SLOT(slotFilterChanged(QtProperty*,QString)));
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtFileEditorFactory::createEditor(QtFilePropertyManager *manager, QtProperty *property, QWidget *parent)
{
    QtFileEdit *editor = d_ptr->createEditor(property, parent);
    editor->setFilter(manager->filter(property));
    editor->setFileMode(manager->fileMode(property));
    editor->setValue(manager->value(property));
    editor->setReadOnly(manager->isReadOnly(property));
    connect(editor, SIGNAL(valueChanged(QString)), this, SLOT(slotSetValue(QString)));
    connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotEditorDestroyed(QObject *)));
    return editor;
}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
QWidget *QtFileEditorFactory::createAttributeEditor(QtFilePropertyManager *manager, QtProperty *property,
                                                    QWidget *parent, Config::BrowserCol attribute)
{
    if (attribute & Config::BrowserCol::CHECK)
    {
        if (!manager->attributeEditable(Config::BrowserCol::CHECK))
            return nullptr;
        QtBoolEdit *editor = d_ptr->createCheckAttributeEditor(property, parent);
        updateCheck(manager, property, editor);

        connect(editor, SIGNAL(toggled(bool)), this, SLOT(slotSetCheck(bool)));
        connect(editor, SIGNAL(destroyed(QObject *)), this, SLOT(slotCheckAttributeEditorDestroyed(QObject *)));
        return editor;
    }
    return nullptr;

}

/*!
 \internal

 Reimplemented from the QtAbstractEditorFactory class.
 */
void QtFileEditorFactory::disconnectPropertyManager(QtFilePropertyManager *manager)
{
    disconnect(manager, SIGNAL(valueChanged(QtProperty*,QString)),
            this, SLOT(slotPropertyChanged(QtProperty*,QString)));
    disconnect(manager, SIGNAL(filterChanged(QtProperty*,QString)),
            this, SLOT(slotFilterChanged(QtProperty*,QString )));
}

QT_END_NAMESPACE

#include "moc_qteditorfactory.cpp"
