from enum import Enum, IntEnum, Flag, IntFlag, auto, unique
from collections import OrderedDict
from io import TextIOWrapper

import numpy as np
import torch as th
from torch.nn import Parameter
from h5py import File, Group
from PySide6 import QtCore
from PySide6.QtCore import QLocale, QPoint, QPointF, QSize, QSizeF, QRect, QRectF, QMutex
from PySide6.QtGui import QCursor, QColor, QFont, QKeySequence
from PySide6.QtWidgets import QSizePolicy

from sknrf.view.widget.qtpropertybrowser.enums import DISPLAY, PropertyID
from sknrf.app.dataviewer.model.dataset import WCArray
from sknrf.utilities.numeric import Info
from .qtpropertybrowser import Config


type_id_map = OrderedDict((
    (bool, PropertyID.BOOL),
    (IntFlag, PropertyID.FLAG),
    (Flag, PropertyID.FLAG),
    (IntEnum, PropertyID.ENUM),
    (Enum, PropertyID.ENUM),
    (int, PropertyID.INT_EDIT),
    (np.int16, PropertyID.INT_EDIT),
    (np.int32, PropertyID.INT_EDIT),
    (np.int64, PropertyID.INT_EDIT),
    (float, PropertyID.DOUBLE_EDIT),
    (np.float16, PropertyID.DOUBLE_EDIT),
    (np.float32, PropertyID.DOUBLE_EDIT),
    (np.float64, PropertyID.DOUBLE_EDIT),
    (complex, PropertyID.COMPLEX_EDIT),
    (np.complex64, PropertyID.COMPLEX_EDIT),
    (np.complex128, PropertyID.COMPLEX_EDIT),
    (WCArray, PropertyID.TF_FILE_EDIT),
    (str, PropertyID.STRING),
    (File, PropertyID.TB_FILE),
    (TextIOWrapper, PropertyID.FILE),
    (QLocale, PropertyID.LOCALE),
    (QPoint, PropertyID.POINT),
    (QPointF, PropertyID.POINTF),
    (QSize, PropertyID.SIZE),
    (QSizeF, PropertyID.SIZEF),
    (QRect, PropertyID.RECT),
    (QRectF, PropertyID.RECTF),
    (QCursor, PropertyID.CURSOR),
    (QColor, PropertyID.COLOR),
    (QFont, PropertyID.FONT),
    (QKeySequence, PropertyID.KEY_SEQUENCE),
    (QSizePolicy, PropertyID.SIZE_POLICY),
    (th.Tensor, PropertyID.LIST),
    (Parameter, PropertyID.LIST),
    (np.ndarray, PropertyID.LIST),
    (list, PropertyID.LIST),
    (tuple, PropertyID.TUPLE),
    (object, PropertyID.PY_OBJECT)
))

domain_id_map = OrderedDict((
    (Config.Domain.TH, PropertyID.LIST),
    (Config.Domain.TF, PropertyID.TF_EDIT),
    (Config.Domain.FF, PropertyID.LIST),
    (Config.Domain.FT, PropertyID.LIST),
    (Config.Domain.TT, PropertyID.LIST),
))

unsupported_types = (QMutex, QtCore.Signal, QtCore.QMetaObject, Info)


def get_attr(model, attribute):
    attribute = int(attribute[1:-1]) if isinstance(model, (list, tuple)) else attribute
    if hasattr(model, "__getitem__"):  # Sequence
        return model.__getitem__(attribute)
    else:  # Object
        return getattr(model, attribute)


def set_attr(model, attribute, value):
    attribute = int(attribute[1:-1]) if isinstance(model, (list, tuple)) else attribute
    if hasattr(model, "__getitem__"):  # Sequence
        model.__setitem__(attribute, value)
    else:  # Object
        setattr(model, attribute, value)


def get_attr_info(model, attribute):
    try:
        info = model.info[attribute]
    except AttributeError:
        ResourceWarning("Attempting to get properties of object type: %s, which does not include Attribute Info"
                        % (type(model),))
        return Info("untitled", write=True)
    except KeyError:
        ResourceWarning("Attempting to get property %s of object type: %s, which does not include Attribute Info"
                        % (attribute, type(model)))
        return Info("untitled", write=True)
    else:
        return info


def model_generator(model, display=DISPLAY.READ):
    # Create a generator based on the model

    def is_check(display_, attribute_, attribute_info_, default_info_):
        return not attribute_.startswith("_") and attribute_info_.get(attribute_, default_info_).check \
            if display_ & DISPLAY.CHECK else False

    def is_public(display_, attribute_, attribute_info_, default_info_):
        return not attribute_.startswith("_") and attribute_info_.get(attribute_, default_info_).read \
            if display_ & DISPLAY.PUBLIC else False

    def is_read(display_, attribute_, attribute_info_, default_info_):
        return attribute_info_.get(attribute_, default_info_).read \
            if display_ & DISPLAY.READ else False

    default_info = Info("???", write=True, check=True)
    if isinstance(model, (dict, Group)):
        dict_, attribute_info = model, {}
    else:  # object
        if hasattr(model, "info"):
            default_info = Info("???", read=False, write=False, check=False)
            dict_, attribute_info = model.info, model.info
        else:
            dict_, attribute_info = model.__dict__, {}
    generator = (k for k in dict_.keys()
                 if is_check(display, k, attribute_info, default_info)
                 or is_public(display, k, attribute_info, default_info)
                 or is_read(display, k, attribute_info, default_info))
    return generator
