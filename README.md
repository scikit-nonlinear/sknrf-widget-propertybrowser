# sknrf-widget-qtpropertybrowser

<img src="qtpropertybrowser.png"><center>Qt RF Property Browser</center>

Modified version of Qt Property Browser for RF Applications.

This module is used to:

    - Render Digital Register values using a scrollable property browser, creating a dynamic GUI that is runtime configurable.
    - Render Analog Signal levels in every confusing notation availble:

        - Format: Re, Re + Imj, Lin < Deg, Log, Deg
        - Scale: Any SI unit
        - Pk/Avg: Peak or Average Level

There are 3-possible Property Browsers:
    - TreePropertyBrowswer
        - Collapsable
    - GroupBoxPropertyBrowser
        - Native Look-And-Feel
    - ButtonPropertyBrowser
        - Collapsable
        - Native Look-And-Feel

## Build-Flow

### Install Dependencies

See requirements.txt

### Release Build

```bash
rm -rf build ; mkdir -p build ; cd build
cmake ..
make
sudo make install
```

### Debug Build
```bash
rm -rf build ; mkdir -p build ; cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DSKNRF_BUILD_DOC=ON -DSKNRF_BUILD_EXTRA=ON -DSKNRF_BUILD_PLUGIN=ON -DSKNRF_BUILD_PY=ON -DSKNRF_BUILD_TEST=ON ..
make
sudo make install
```

### Example

```bash
python3 main.py
```

### Tests

```bash
qtpropertybrowsertest
```

## Property Managers and Editor Factories

<table>
    <tr>
        <th> C++ Type
        <th> Python Type
        <th> Property Manager
	    <th> Editor Factory1
	    <th> Editor Factory2
	    <th> Editor Factory3
	    <th> Editor Factory4
    </tr>
    <tr>
        <td> int</td>
        <td> int</td>
        <td> QtIntPropertyManager</td>
	    <td> QtIntEditFactory</td>
	    <td> QtSpinBoxFactory</td>
	    <td> QtSliderFactory</td>
    </tr>
    <tr>
        <td> bool</td>
        <td> </td>
	    <td> QtBoolPropertyManager</td>
        <td> QtCheckBoxFactory</td>
    </tr>
    <tr>
        <td> double</td>
        <td> double</td>
	    <td> QtDoublePropertyManager</td>
        <td> QtDoubleEditFactory</td>
	    <td> QtDoubleSpinBoxFactory</td>
    </tr>
    <tr>
        <td> QtComplex</td>
        <td> complex</td>
	    <td> QtComplexPropertyManager</td>
        <td> QtComplexEditFactory</td>
    </tr>
    <tr>
        <td> QtQuaternion</td>
	    <td> *</td>
	    <td> QtQuaternionPropertyManager</td>
        <td> QtQuaternionEditFactory</td>
    </tr>
    <tr>
        <td> QVector&lt;QtComplex&gt;</td>
        <td> array</td>
	    <td> QtVectorComplexPropertyManager</td>
	    <td> QtVectorComplexEditFactory</td>
    </tr>
    <tr>
        <td> QString</td>
        <td> str</td>
        <td> QtStringPropertyManager</td>
        <td> QtLineEditFactory</td>
    </tr>
    <tr>
        <td> QDate</td>
        <td> QDate</td>
        <td> QtDatePropertyManager</td>
        <td> QtDateEditFactory</td>
    </tr>
    <tr>
        <td> QTime</td>
        <td> QTime</td>
        <td> QtTimePropertyManager</td>
        <td> QtTimeEditFactory</td>
    </tr>
    <tr>
        <td> QDateTime</td>
        <td> QDateTime</td>
        <td> QtDateTimePropertyManager</td>
        <td> QtDateTimeEditFactory</td>
    </tr>
    <tr>
        <td> QKeySequence</td>
        <td> QKeySequence</td>
        <td> QtKeySequencePropertyManager</td>
        <td> QtKeySequenceEditorFactory</td>
    </tr>
    <tr>
        <td> QChar</td>
        <td> QChar</td>
        <td> QtCharPropertyManager</td>
        <td> QtCharEditorFactory</td>
    </tr>
    <tr>
        <td> QPoint</td>
        <td> QPoint</td>
        <td> QtPointPropertyManager</td>
        <td> QtPointEditorFactory</td>
    </tr>
    <tr>
        <td> QPointF</td>
        <td> QPointF</td>
        <td> QtPointFPropertyManager</td>
        <td> QtPointFEditorFactory</td>
    </tr>
    <tr>
        <td> QSize</td>
        <td> QSize</td>
        <td> QtSizePropertyManager</td>
        <td> QtSizeEditorFactory</td>
    </tr>
    <tr>
        <td> QSizeF</td>
        <td> QSizeF</td>
        <td> QtSizeFPropertyManager</td>
	    <td> QtSizeFEditorFactory</td>
    </tr>
    <tr>
	    <td> QRect</td>
	    <td> QRect</td>
        <td> QtRectPropertyManager</td>
        <td> QtRectFEditorFactory</td>
    </tr>
    <tr>
        <td> QEnum</td>
        <td> QEnum</td>
        <td> QtEnumPropertyManager</td>
        <td> QtEnumEditorFactory</td>
    </tr>
    <tr>
        <td> QFlag</td>
        <td> QFlag</td>
        <td> QtFlagPropertyManager</td>
        <td> QtFlagEditorFactory</td>
    </tr>
    <tr>
        <td> QLocale</td>
        <td> QLocale</td>
        <td> QtLocalePropertyManager</td>
        <td> QtLocaleEditorFactory</td>
    </tr>
    <tr>
        <td> QSizePolicy</td>
        <td> QSizePolicy</td>
        <td> QtSizePolicyPropertyManager</td>
        <td> QtSizePolicyEditorFactory</td>
    </tr>
    <tr>
        <td> QFont</td>
        <td> QFont</td>
        <td> QtFontPropertyManager</td>
        <td> QtFontEditorFactory</td>
    </tr>
    <tr>
	    <td> QColor</td>
	    <td> QColor</td>
        <td> QtColorPropertyManager</td>
        <td> QtColorEditorFactory</td>
    </tr>
    <tr>
        <td> QCursor</td>
        <td> QCursor</td>
        <td> QtCursorPropertyManager</td>
        <td> QtCursorEditorFactory</td>
    </tr>
    <tr>
        <td> QFile</td>
        <td> QFile</td>
        <td> QtFilePropertyManager</td>
        <td> QtFileEditorFactory</td>
    </tr>
</table>


.* C++ only, no Python datatype

## Original Source Code:

- Repo: [qt/qttools](https://code.qt.io/cgit/qt/qttools.git/)
- Project: [qt/qttools/tree/src/sharedqtpropertybrowswer](https://code.qt.io/cgit/qt/qttools.git/tree/src/shared/qtpropertybrowser)
- Qt Version: 6.5

## Original License

This Property Browser has features that are specific to the field of RF.

    - Floating-Point Rendering of Fixed-Point ADC/DAC sensor.
    - Complex Number Support
    - Array (QVector<QtComplex>) Support.

I try very hard to keep the code easy to diff with the Qt source-base. See warning below:

```cpp
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Designer.  This header
// file may change from version to version without notice, or even be removed.
//
// We mean it.
//
//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Designer.  This header
// file may change from version to version without notice, or even be removed.
//
// We mean it.
//
```

## Floating-Point Rendering of Fixed-Point ADC/DAC sensor.

Software uses a floating-point data-type based on the [IEEE Standard for Floating-Point Arithmetic (IEEE 754)](https://en.wikipedia.org/wiki/IEEE_754).
A 32-bit IEEE 754 floating-point has the following bit encoding:
    - 1 Sign-bit
    - 8 Exponent-bits
    - 23 Fraction-bits

### Fraction-Bits
If we force the exponent-bits to equal 0, the fraction-bits would represent a 23-bit precision DAC/ADC and the absolute
tolerance would be:

$$ atol = (Max - Min) / 2^Pecision$$
    
Hence, if we have **23-bit precision** sensor *bounded* by a **Max**/**Min** voltage of **+1V/-1V**, the absolute 
tolerance is:

$$ atol = (+1V - -1V) / 2^23 = 2.384185791015625e-07V$$

Hence, the minimum voltage step our 23-bit sensor can measure is 2.384185791015625e-07V. The [numpy.isclose](https://numpy.org/doc/stable/reference/generated/numpy.isclose.html)
function has a default **atol=1e-08** indicating that 32-bit float can achieve slightly better accuracy when it uses the exponent
bits.

### Exponent-Bits

The exponent-bits add a scaling factor that allows the same 23 fraction-bits to represent very large or small values.
Since the IEEE 754 float approximates a decimal number using *two separte numbers*, we need a second tolerance **rtol** to 
define the accuracy of the exponent-bits when the number is very large. This is why [numpy.isclose](https://numpy.org/doc/stable/reference/generated/numpy.isclose.html)
requires both a absolute tolerance (*atol*) and relative tolerance (*rtol*). 

### Application to Analog Circuits

Any analog sensor connected to a digital microprocessor/microcontroller must quantize the signal and store the data as
an integer typically defined by:
    - 0 or 1 Sign-bit
    - N or N - 1 Fraction-bits

In this library, we render only the *peak or average* voltage of a waveform using 64-bit floats. Hence, the value rendered
on the screen is in no-way limited by the precision of the computer. Therefore, the datasheet of the sensor fully defines 
the floating-point precision of the number rendered on the screen.

**When rendering floating-point measurements on the screen, you must define the:**
    - Max (using *property_manager.setMaximum(property, max)* or *property_manager.setRange(property, min, max)*)
    - Min (using *property_manager.setMinimum(property, min)* or *property_manager.setRange(property, min, max)*)
    - Precision (using *.setPrecision()*)

After the **Min**, **Max**, and **Precision** inputs are defined, the following **read-only** methods are offered as a 
convenience when using software to check numerical accuracy.
    - **atol** (using *.atol()*): The absolute tolerance
    - **rotl_min** (using *.rtol()*): The minimum relative tolerance

### Illustrative Example

If you are using [numpy.isclose](https://numpy.org/doc/stable/reference/generated/numpy.isclose.html), you should call

```python3
    import numpy
    if numpy.isclose(a, b, property_manager.atol(property), property_manager.rtol(property)):
        print('a == b')
    else:
        print('a != b')
```

### Equations

The abolute tolerance (*atol*) is calculated using:

$$atol = (maximum.real() - minimum.real()) / (1 << precision);$$

The minimum relative tolerance (*rtol*) is calculated using:

$$rtol = 1 / 1 << precision$$

When you manually enter a number:
  
   - A Maximum limit is applied to prevent damage to the hardware.
   - A Minimum limit is applied to prevent numerical underflow (uncommon).
   - A isclose function determines if a numerical change would produce a change in the state of the hardware.
       - This blocks unecessary I/O communcation.
       - This prevents I/O ringing where the state of the fixed-point hardware is slightly different that the floating point software.

The `TreePropertyBrowser` is specifically challanged by I/O ringing because the `PropertyEditor` is momentarily superimposed on top of the `PropertyManager`. Even with this unique challenge, the TreePropertyBrowser has the following advantages:
   
    - Colapsable
    - Click-to-Edit blocks most erroroneous user input into GUI widgets that are sensitive to:
        - Keyboard Typos
        - Inacurate Mouse Clicks
        - Unintentional Mouse Scrolling
        - Unintentional Touch Screen Gesturing
    - Most area efficient rendering on a small embedded screen

As it is desirable to efficiently render all information about the hardware on the screen, the number of significant digits displayed is automatically calculated from the absolute tolerance using the following equation.

    - Foramt == Format::LOG_*
	$$scaled_val = val/sqrt(pow(10, scale_))$$
    - Otherwise
	$$scaled_val = val/pow(10, scale_)$$

    $$num_sig_fig = max(-floor(log10(ftol)), 1)$$

The following parameters should be determined by the hardware:

    - precision
    - minimum
    - maximum

While the following parameters can be modified dynamically to minimize the number of significant figures.

    - scale
    - format
        

## Complex Numbers

An RF signal is typically defined using:

    - A Complex phasor defining the Magnitude and Phase of the Carrier Signal
    - A Time-Varying Complex Phase defining the Magnitude and Phase of the Message Signal

Signal **x(t_m, f_c)**, a signal that varies with modulation time (**t_m**) and carrier frequency (**f_c**) is defined as:

$$x(t,f) = A*exp(j*2*pi*f_c*t)*[I(t)*cos(2*pi*f_m*t) - Q(t)*1j*sin(2*pi*f_m*t)]$$

If you look closely, you can see to variables that define frequency:

    - **f_c**: The Frequency of the Carrier Signal (GHz)
    - **f_m**: The Frequency of the Message Signal (MHz)

This is called a *Multi-Rate Envelope Modulated System*.

    - The Carrier signal is typically represented as a complex phasor in the frqency domain wrt **f_c**.
    - The Message signal can be sampled by 2 sampling circuits (I and Q) Channel in the time domain wrt **t_m**.

We typically measure:
    - The carrier signal during device-level characterization.
    - The message signal during system-level characterization.

Device-Level measurements require a static complex phasor representation, while System-Level measurements typically only
show the magnitude because the phase is time-varying.

Always remember that phase is in the frequency domain is equivalent to a delay in the time-domain.

## Array (QVector<QtComplex>) Support.

Sometimes digital sampling is not fast enough to approximate analog behaviour. In these situations we can:
    
    - Approximate a square-wave by superimposing sine waves with odd harmonics.
    - Approximate a sawtooth-wave by superimposing sine waves with even harmonics.

In general, any periodic waveform can be approximated using multiple harmonically related sine-waves *as long as the
relative phase between each harmonic sine wave is well-defined*. All possible waveforms are described by properties of
the [Fourier Series](https://en.wikipedia.org/wiki/Fourier_series).

The `QVectorPropertyManager` renders the `QVector<QtComplex>` data type, which is very similar to `std::vector< std::complex >`.

